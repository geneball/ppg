
JEB.drawing = function(canvas, options){
    this.name = canvas.id;
    this.canvas = canvas;
    this.width = canvas.width;
    this.height = canvas.height;
    this.radius = Math.min(this.width, this.height) / 2.0;
    this.cx = this.width / 2.0;
    this.cy = this.height / 2.0;
    this.ctx = canvas.getContext('2d');
    return this.init(options);
};
JEB.drawing.self = 0;
JEB.drawing.styles = {};
JEB.drawing.prototype = {
    init: function(options){
        drawing.self = this;
        this.bgcolor = '#dddddd';
        this.fgcolor = '#FFFFFF';
        this.children = [];
        JEB.setOptions(this, options, drawing.styles);
        if (this.refreshTicks) 
            this.refresh(this.refreshTicks);
        return this;
    },
    render: function(){
        if (!this.ctx || document.getElementById(this.name) == null) 
            return;
        this.ctx.globalAlpha = 1.0;
        this.ctx.strokeStyle = this.fgcolor;
        this.ctx.fillStyle = this.bgcolor;
        this.ctx.fillRect(0, 0, this.width, this.height);
        for (var i in this.children) 
            this.children[i].render(this.ctx);
        return this;
    },
    refresh: function(tics){
        drawing.self.refreshTics = tics;
        drawing.self.render();
        window.setTimeout(function(){
            JEB.drawing.prototype.refresh(tics)
        }, tics);
    },
    erase: function(){
        this.children = [];
    },
    pctToX: function(x){ // maps 0..100 into canvas x
        return this.width * x / 100;
    },
    pctToY: function(y){ // maps 0..100 into canvas y
        return this.height * (100 - y) / 100;
    },
    pctToSz: function(d){
        return Math.min(this.width, this.height) * d / 100;
    },
    pctToCrds: function(x, y, d1, d2){
        return {
            x: this.pctToX(x),
            y: this.pctToY(y),
            d1: this.pctToSz(d1),
            d2: this.pctToSz(d2)
        };
    }
}
JEB.rect = function(drawing, par, options){
    this.drawing = drawing;
    this.parent = par;
    par.children.push(this);
    return this.init(options);
}
JEB.rect.cnt = 0;
JEB.rect.styles = {};
JEB.rect.prototype = {
    init: function(options){ // all units in %
        this.name = 'rect' + rect.cnt++;
        this.wide = 5;
        this.high = 5;
        this.lx = 47.5;
        this.ty = 52.5;
        this.draw = true;
        this.lineColor = "#000000";
        this.lineWidth = 2;
        this.fill = false;
        this.fillColor = "#ff0000";
        this.fields = "name;wide;high;lx;ty;draw;linecolor;linewidth;fill;fillcolor";
        JEB.setOptions(this, options, rect.styles);
        return this;
    },
    render: function(ctx){
		if (this.hidden) return;
        ctx.save();
        ctx.fillStyle = this.fillColor;
        var crds = this.drawing.pctToCrds(this.lx, this.ty, this.wide, this.high);
        var ty = this.dr
        if (this.fill) 
            ctx.fillRect(crds.x, crds.y, crds.d1, crds.d2);
        
        ctx.strokeStyle = this.lineColor;
        ctx.lineWidth = this.lineWidth;
        if (this.draw) 
            ctx.strokeRect(crds.x, crds.y, crds.d1, crds.d2);
        
        if (this.text) {
            ctx.textBaseline = "top";
            ctx.font = crds.d2 * .6 + "pt Arial";
            ctx.fillStyle = this.lineColor;
            var txt = this.text;
            if (typeof txt == 'function') 
                txt = txt();
            ctx.fillText(txt, crds.x + 2, crds.y);
        }
        ctx.restore();
        return this;
    }
};

JEB.marker = function(drawing, par, options){
    this.drawing = drawing;
    this.parent = par;
    par.children.push(this);
    return this.init(options);
}
JEB.marker.cnt = 0;
JEB.marker.styles = {};
JEB.marker.prototype = {
    init: function(options){
        this.name = 'marker' + marker.cnt++;
        this.draw = true;
        this.linecolor = "#58b0AA";
        this.fill = false;
        this.fillcolor = "#A725D3";
        this.cx = this.parent.cx;
        this.cy = this.parent.cy;
        this.irad = 5;
        this.orad = 10;
        this.npts = 5;
        this.nloops = 3;
        JEB.setOptions(this, options, marker.styles);
        this.step = 360 * this.nloops / this.npts;
        return this;
    },
    render: function(ctx){
		if (this.hidden) return;
        ctx.save();
        ctx.beginPath();
        var cx = this.cx;
        var cy = this.cy;
        var irad = this.irad;
        var orad = this.orad;
        var npts = this.npts;
        var step = this.step * Math.PI / 180;
        var crds = this.drawing.pctToCrds(cx, cy, irad, orad);
        ctx.moveTo(crds.x + crds.d2, crds.y);
        var a = step;
        for (var i = 0; i < npts; i++) {
            var rad = (i % 2 == 0) ? crds.d1 : crds.d2;
            var ax = crds.x + rad * Math.cos(a);
            var ay = crds.y + rad * Math.sin(a);
            ctx.lineTo(ax, ay);
            a += step;
        }
        ctx.closePath();
        ctx.strokeStyle = this.linecolor;
        ctx.fillStyle = this.fillcolor;
        if (this.draw) 
            ctx.stroke();
        if (this.fill) 
            ctx.fill();
        ctx.restore();
    }
}

JEB.fraction = function(drawing, par, scale, options){
    this.drawing = drawing;
    this.parent = par;
    this.scale = scale;
    par.children.push(this);
    return this.init(options);
}
JEB.fraction.styles = {};
JEB.fraction.cnt = 0;
JEB.fraction.prototype = {
    init: function(options){
        this.name = 'fraction' + fraction.cnt++;
        this.draw = false;
        this.lineColor = "#ff0000";
        this.fill = true;
        this.fillColor = "#00ffff";
        this.cx = this.parent.cx;
        this.cy = this.parent.cy;
        JEB.setOptions(this, options, fraction.styles);
        return this;
    },
    render: function(ctx){
		if (this.hidden) return;
        ctx.save();
        ctx.globalAlpha = 1.0;
        ctx.strokeStyle = this.lineColor;
        ctx.fillStyle = this.fillColor;
		this.scale.drawTic(ctx, this.ticStart, this.ticLen, this.scaleVal(0), this.scaleVal(this.value), this.draw, this.fill, 'end');
        ctx.restore();
    },
	scaleVal: function(val){
		var vnorm = (val-this.scale.minValue)/(this.scale.maxValue-this.scale.minValue);  // 0..1
		return this.scale.minTic + vnorm * this.scale.ticRange; 
	}
}

JEB.scale = function(drawing, par, options){
    this.drawing = drawing;
    this.parent = par;
    par.children.push(this);
    return this.init(options);
}
JEB.scale.cnt = 0;
JEB.scale.styles = {
		vert0to100by10: 'style:vertical;minTic:0;maxTic:100;ticStep:10;ticAlign:center;ticStart:3;ticLen:5;drawBase:true;',
		vert10to90by5: 'style:vertical;minTic:10;maxTic:90;ticStep:5;ticAlign:center;ticStart:3;ticLen:5;drawBase:true;',
		horiz0to100by10: 'style:horizontal;minTic:0;maxTic:100;ticStep:10;ticAlign:center;ticStart:3;ticLen:5;drawBase:true;',
		horiz10to90by5: 'style:horizontal;minTic:10;maxTic:90;ticStep:5;ticAlign:center;ticStart:3;ticLen:5;drawBase:true;',
		clock0to100by10: 'style:clockwise;minTic:0;maxTic:100;ticStep:10;ticAlign:center;ticStart:3;ticLen:5;drawBase:true;',
		clock10to90by5: 'style:clockwise;minTic:10;maxTic:90;ticStep:5;ticAlign:center;ticStart:3;ticLen:5;drawBase:true;',
		ccw0to100by10: 'style:counter-cw;minTic:0;maxTic:100;ticStep:10;ticAlign:center;ticStart:3;ticLen:5;drawBase:true;',
		ccw10to90by5: 'style:counter-cw;minTic:10;maxTic:90;ticStep:5;ticAlign:center;ticStart:3;ticLen:5;drawBase:true;',
	};
JEB.scale.prototype = {
    init: function(options){ //'radius:100;cx:50;cy:50;style:vertical;minTic:0;maxTic:100;ticStep:10;ticStart:0;ticLen:5;ticWidth:1;ticAlign:center;draw:false;lineColor:#0000FF;fill:true;fillColor:#1010E0;'
        this.name = 'scale' + scale.cnt++;
		this.minValue = 0;
		this.maxValue = 100;  
        // scale area is centered at cx,cy with given radius
        this.radius = 100;
        this.cx = 50;
        this.cy = 50;
        this.style = "vertical"; // horizontal,clockwise,counter-cw
        // tics go from minTic..maxTic in steps of ticStep
        this.minTic = 0;
        this.maxTic = 100;
        this.ticStep = 10; // % to next tic
        // each tic begins at 'ticStart' and goes for 'ticLen' with thickness 'ticWidth'
        this.ticStart = 0; // % from left,bottom,center
        this.ticLen = 5;
        this.ticWidth = 1; // of each tic, in % value
        this.ticAlign = 'center';
        this.draw = true;
        this.lineColor = "#0000FF";
        this.fill = true;
        this.fillColor = "#1010E0";
        this.useredit = "style:r:vertical,horizontal,clkwise,ccwise;minValue:v;maxValue:v;ticStart:v;ticLen:v;ticWidth:v;ticStep:v;lineColor:c;fillColor:c";
        JEB.setOptions(this, options, scale.styles);
		this.ticRange = this.maxTic - this.minTic;
        return this;
    },
    render: function(ctx){
		if (this.hidden) return;
        ctx.save();
        ctx.globalAlpha = 1.0;
        ctx.strokeStyle = this.lineColor;
        ctx.fillStyle = this.fillColor;
		if (this.drawBase) 
			this.drawTic(ctx, this.ticStart-this.ticWidth, this.ticWidth, this.minTic, this.ticRange+this.ticWidth, this.draw, this.fill, 'start' );
        var tic = this.minTic; // gives 'start' alignment
        while (tic <= this.maxTic) {
            var ticH = tic + this.ticWidth;
            this.drawTic(ctx, this.ticStart, this.ticLen, tic, this.ticWidth, this.draw, this.fill, this.ticAlign);
            tic += this.ticStep;
        }
        ctx.restore();
    },
    // draw 'thick' by 'len' tic aligned at 'val', starting at 'offset' (all in %value or %radius)
    //  depends on scale attributes 'style', 'cx','cy','radius', 'ticAlign', 'lineColor', 'fillColor'
    drawTic: function(ctx, offset, len, val, thick, draw, fill, ticAlign){
        switch (ticAlign) {
            case 'start':
                break;
            default:
            case 'center':
                val -= thick / 2;
                break;
            case 'end':
                val -= thick;
                break;
        }
        var cx = this.cx;
        var cy = this.cy;
        var irad = (100 - offset - len) * this.radius / 100;
        var orad = (100 - offset) * this.radius / 100;
        switch (this.style) {
            case 'vertical': // offset->lx,val->ty(*align),len->wide,thick->high
                this.scRectangle(ctx, offset, val+thick, len, thick, this.draw, this.fill);
                break;
            case 'horizontal': // val->lx(*align), offset+len->ty, thick->wide, len->high
                this.scRectangle(ctx, val, offset + len, thick, len, this.draw, this.fill);
                break;
            case 'clockwise': // val->start angle(*align), val+thick->end angle, offset->inner radius, offset+len->outer radius
                this.scArcBand(ctx, cx, cy, irad, orad, val, val + thick, false, this.draw, this.fill);
                break;
            case 'counter-cw':
                this.scArcBand(ctx, cx, cy, irad, orad, val, val + thick, true, this.draw, this.fill);
                break;
        }
    },
    scArcBand: function(ctx, cx, cy, irad, orad, ang1, ang2, ccw, draw, fill){
        var crds = this.drawing.pctToCrds(cx, cy, irad / 2, orad / 2);
        var pctToRad = 2 * Math.PI / 100;
        ang1 *= pctToRad;
        ang2 *= pctToRad;
        ctx.beginPath();
        if (ccw) {
            ctx.arc(crds.x, crds.y, crds.d2, -ang1, -ang2, ccw); // outer arc from ang1 to ang2
            ctx.arc(crds.x, crds.y, crds.d1, -ang2, -ang1, !ccw);
        }
        else {
            ctx.arc(crds.x, crds.y, crds.d2, ang1, ang2, ccw); // outer arc from ang1 to ang2
            ctx.arc(crds.x, crds.y, crds.d1, ang2, ang1, !ccw);
        }
        ctx.closePath();
        if (draw) 
            ctx.stroke();
        if (fill) 
            ctx.fill();
    },
    scRectangle: function(ctx, x1, y1, w, h, draw, fill){
        var crds = this.drawing.pctToCrds(x1, y1, w, h);
        if (draw) 
            ctx.strokeRect(crds.x, crds.y, crds.d1, crds.d2);
        if (fill) 
            ctx.fillRect(crds.x, crds.y, crds.d1, crds.d2);
    },
//    scRect: function(ctx, x1, y1, x2, y2, draw, fill){
//        var pt1 = this.drawing.pctToCrds(this.x1, this.y1);
//        var pt2 = this.drawing.pctToCrds(this.x2, this.y2);
//        x1 *= this.width;
//        y1 = (1.0 - y1) * this.height;
//        x2 *= this.width;
//        y2 = (1.0 - y2) * this.height;
//        if (draw) 
//            ctx.strokeRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
//        if (fill) 
//            ctx.fillRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
//    },
//    scArc: function(ctx, x, y, irad, orad, ang1, ang2, ccw, draw, fill){
//        ctx.beginPath();
//        if (ccw) {
//            var ix2 = x + orad * Math.cos(ang1);
//            var iy2 = y - orad * Math.sin(ang1);
//            
//            ctx.arc(x, y, irad, ang2, ang1, ccw);
//            ctx.lineTo(ix2, iy2);
//            ctx.arc(x, y, orad, ang1, ang2, !ccw);
//        }
//        else {
//            var ix2 = x + orad * Math.cos(-ang1);
//            var iy2 = y - orad * Math.sin(-ang1);
//            
//            ctx.arc(x, y, irad, ang1, ang2, ccw);
//            ctx.lineTo(ix2, iy2);
//            ctx.arc(x, y, orad, ang2, ang1, !ccw);
//        }
//        ctx.closePath();
//        if (draw) 
//            ctx.stroke();
//        if (fill) 
//            ctx.fill();
//    }
}

JEB.timebase = function(options){ // options: "epoch:0;units:standard/grallt/swatch;"
    return this.init(options);
};
JEB.timebase.cnt = 0;
JEB.timebase.styles = {};
JEB.timebase.standard = ['name:day;msec:86400000;min:1;max:31;fields:*;', 'name:hour;msec:3600000;min:0;max:23;fields:*;', 'name:minute;msec:60000;min:0;max:59;fields:*;', 'name:second;msec:1000;min:0;max:59;fields:*;'];
JEB.timebase.grallt = ['name:zul;msec:6912000000;min:0;max:5;fields:*;', 'name:llor;msec:108000000;min:1;max:64;fields:*;', 'name:ande;msec:18000000;min:1;max:8;fields:*;', 'name:utle;msec:2250000;min:0;max:63;fields:*;', 'name:tle;msec:35156.25;min:0;max:63;fields:*;', 'name:antle;msec:549.31640625;min:0;max:63;fields:*;'];
JEB.timebase.swatch = ['name:day;msec:86400000;min:1;max:31;fields:*;', 'name:beat;msec:86400;min:0;max:999;fields:*;', 'name:centibeat;msec:864;min:0;max:99;fields:*;'];
JEB.timebase.bmt = ['name:day;msec:86400000;min:1;max:31;fields:*;', 'name:bmthour;msec:3600000;min:0;max:23;fields:*;', 'name:minute;msec:60000;min:0;max:59;fields:*;', 'name:second;msec:1000;min:0;max:59;fields:*;'];
JEB.timebase.prototype = {
    init: function(options){
        this.name = 'timebase' + timebase.cnt++;
        this.units = timebase.standard; // array of unit desciptors
        this.epoch = 0;
        this.fields = 'name;units;epoch;';
        JEB.setOptions(this, options, timebase.styles);
        var system = 'standard';
        if (typeof this.units == 'string') {
            system = this.units;
            switch (this.units) {
                case 'grallt':
                    this.units = timebase.grallt;
                    this.fmt = '{0}/{1}.{2} {3}:{4}:{5}';
                    break;
                case 'swatch':
                    this.units = timebase.bmt;
                    this.fmt = '{0} @{1}.{2}';
                    break;
                case 'bmt':
                    this.units = timebase.bmt;
                    this.fmt = '{0} {1}:{2}:{3}'
                    break;
                default:
                case 'standard':
                    this.units = timebase.standard;
                    this.fmt = '{0} {1}:{2}:{3}'
                    break;
            }
        }
        this.units = this.createUnits(this.units); // cvt to array of objects
        var nw = new Date();
        this.update(nw.getTime());
        if (this.units[0].name == 'day') {
            var hr = nw.getHours();
            var dy = nw.getDate();
            if (this.units[1].name == 'bmthour') {
                var hr = nw.getUTCHours() + 1;
                var dy = nw.getUTCDate();
                if (hr > 23) {
                    hr -= 24;
                    dy++;
                }
            }
            var hroff = (this.units[1].value - hr) * this.units[1].msec;
            var dyoff = (this.units[0].value - dy) * this.units[0].msec;
            this.epoch = hroff + dyoff;
            if (system == 'swatch') 
                this.units = this.createUnits(timebase.swatch);
        }
        return this;
    },
    createUnits: function(optarray){
        var un = [];
        for (var i in optarray) 
            un.push(JEB.optsToObj(optarray[i]));
        return un;
    },
    update: function(timeinmsec){
        if (!timeinmsec) 
            timeinmsec = (new Date().getTime());
        timeinmsec -= this.epoch;
        for (var i in this.units) {
            var unit = this.units[i];
            var rng = (unit.max - unit.min + 1);
            var v = Math.floor((timeinmsec / unit.msec) % rng);
            unit.fraction = v / rng;
            unit.value = ('000' + String(unit.min + v)).slice(-(String(unit.max).length));
        }
    },
    toCurrString: function(){
        return this.toString(true);
    },
    getFmtFunc: function(){
        var self = this;
        toFmt = function(){
            self.update();
            switch (self.units.length) {
                case 3:
                    return self.fmt.format(self.units[0].value, self.units[1].value, self.units[2].value);
                case 4:
                    return self.fmt.format(self.units[0].value, self.units[1].value, self.units[2].value, self.units[3].value);
                case 5:
                    return self.fmt.format(self.units[0].value, self.units[1].value, self.units[2].value, self.units[3].value, self.units[4].value);
                case 6:
                    return self.fmt.format(self.units[0].value, self.units[1].value, self.units[2].value, self.units[3].value, self.units[4].value, self.units[5].value);
            }
        };
        return toFmt;
    },
    toString: function(currTime){
        if (currTime) 
            this.update();
        var s = "";
        for (var i in this.units) 
            s += this.units[i].name + ' ' + this.units[i].value + ', ';
        return s;
    }
}

JEB.clock = function(drawing, par, options){ // options: ""
    this.drawing = drawing;
    this.parent = par;
    par.children.push(this);
    return this.init(options);
}
JEB.clock.cnt = 0;
JEB.clock.styles = {};
JEB.clock.prototype = {
    init: function(options){
        this.name = 'clock' + clock.cnt++;
        this.scales = [];
        this.fracts = [];
        this.units = 'standard';
        JEB.setOptions(this, options, clock.styles);
        var tb = new timebase('units:' + this.units + ';');
        var drw = this.drawing;
        if (this.showtext) {
            var ct = new rect(drw, drw, 'lx:5.5;ty:6;wide:90;high:5;fillcolor:#a0a0ef;draw:false;fill;');
            ct.text = tb.getFmtFunc();
        }
        var refms = this.units[this.units.length - 1].msec;
        for (var i in this.units) 
            if (this.units[i] == this.refresh) 
                refms = this.units[i].msec;
        drw.refresh(1000);
        return this;
    },
    render: function(){ 
    }
};

JEB.initDrawing(canvasID){
    var canvas = document.getElementById(canvasID);
    if (!canvas) 
        return;
    JEB.drw = new drawing(canvas);
}	
/*
    $('#btnH').live('click', function(){
        drw.erase();
        for (i = 0; i < 20; i++) 
            var r = new rect(drw, drw, 'lx:Rnd(0..80);ty:Rnd(20..100);wide:Rnd(5..30);high:Rnd(5..30);randInWhite;');
        drw.render();
        //		alert( JEB.objToOpts(tb));
    });
    $('#btnG').live('click', function(){
        drw.erase();
        for (i = 0; i < 20; i++) 
            new marker(drw, drw, 'cx:Rnd(10..90);cy:Rnd(10..90);npts:RndI(3..19);nloops:RndI(1..20);');
        drw.render();
    });
    $('#btnI').live('click', function(){
        drw.erase();
        //'radius:100;cx:50;cy:50;style:vertical;minTic:0;maxTic:100;ticStep:10;ticStart:0;ticLen:5;ticWidth:1;ticAlign:center;draw:false;lineColor:#0000FF;fill:true;fillColor:#1010E0;'
        var vsc = new scale(drw, drw, 'vert10to90by5;RndOpt(blueInWhite,greenInWhite,redInBlack,yellow);'); 
        var hsc = new scale(drw, drw, 'horiz10to90by5;fillColor:RndOpt(green,blue,yellow,white,black);');
        var cwsc = new scale(drw, drw, 'clock10to90by5;radius:85;red;');
        var ccwsc = new scale(drw, drw, 'ccw10to90by5;radius:65;yellow;');

        var f1 = new fraction(drw, drw, vsc, 'ticStart:10;ticLen:10;value:50;blueInWhite;');
        var f2 = new fraction(drw, drw, hsc, 'ticStart:10;ticLen:5;value:50;green;');
        var f3 = new fraction(drw, drw, cwsc, 'ticStart:9;ticLen:8;value:50;red;');
        var f4 = new fraction(drw, drw, ccwsc, 'ticStart:10;ticLen:8;value:50;yellow;');
        
        drw.render();
    });
    $('#btnJ').live('click', function(){
        drw.erase();
        var tb = new timebase('units:grallt;');
        tb.update((new Date()).getTime());
        new rect(drw, drw, 'lx:5;wide:90;high:5;fillcolor:#ff0000;linecolor:#000000;fill;text:' + tb.toString());
        drw.render();
    });
    $('#btnK').live('click', function(){
        drw.erase();
        var tb = new timebase('units:swatch;');
        var ct = new rect(drw, drw, 'lx:5;wide:90;high:5;fillcolor:#80f080;linecolor:#000000;fill;');
        ct.text = tb.getFmtFunc();
        drw.refresh(864);
    });
    $('#btnL').live('click', function(){
        drw.erase();
        var tb = new timebase('units:grallt;');
        var ct = new rect(drw, drw, 'lx:5;wide:90;high:5;fillcolor:#efa0a0;linecolor:#000000;fill;');
        ct.text = tb.getFmtFunc();
        drw.refresh(519);
    });
    $('#btnC').live('click', function(){
        drw.erase();
        var tb = new timebase('units:standard;');
        var ct = new rect(drw, drw, 'lx:5.5;ty:6;wide:90;high:5;fillcolor:#a0a0ef;draw:false;fill;');
        ct.text = tb.getFmtFunc();
        drw.refresh(1000);
    });
    $('#btnM').live('click', function(){
        drw.erase();
        new clock(drw, drw, 'units:standard;refresh:minute;showtext;');
    });
*/
    
    

