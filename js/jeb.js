/* Author:
 Gene Ball   Aug 2012
 */
String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{' + i + '\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};
$.fn.listHandlers = function(events, outputFunction) {
    return this.each(function(i) {
        var elem = this, dEvents = $._data(elem, "events");
        //            dEvents = $(this).data('events');
        if (!dEvents)
            return;
        $.each(dEvents, function(name, handler) {
            if ((new RegExp('^(' + (events === '*' ? '.+' : events.replace(',', '|').replace(/^on/i, '')) + ')$', 'i')).test(name)) {
                $.each(handler, function(i, handler) {
                    console.info(elem, '\n' + i + ': [' + name + '] : ' + handler);
                });
            }
        });
    });
};
var JEB = {
    Debug : true,
    Header : true,
    dbg : function(cd, msg) {
        if (!JEB.Debug)
            return;
        var s = JEB.Version + ' ' + cd + ' ' + msg;
        if (JEB.getDbg(cd)) {
            JEB.dbgLog = s + "<br>" + JEB.dbgLog;
            if (JEB.dbgLog.length > 2000)
                JEB.dbgLog = JEB.dbgLog.substr(0, 2000);
            if (JEB.Header)
                JEB.setHeader(s);
            if (window.console)
                console.log(s);
            $("#jeb-Debug").html(JEB.dbgLog);
            if (cd == 'alert')
                alert(s);
        }
    },
    getDbg : function(cd) {
        if (JEB.dbgEnable[cd] == undefined)
            JEB.dbgEnable[cd] = true;
        return JEB.dbgEnable[cd];
    },
    setDbg : function(cd, enable) {
        JEB.dbgEnable[cd] = enable;
    },
    dbgMenu : ['Debug', 1, 'Show;Hide;Header;Disable;User'],
    dbgCmd : function(cmd) {
        switch (cmd) {
            case 'Show':
                $('#jeb-Debug').show('slow');
                break;
            case 'Hide':
                $('#jeb-Debug').hide('fast');
                break;
            case 'Header' :
                JEB.Header = !JEB.Header;
                break;
            case 'Disable':
                JEB.Debug = !JEB.Debug;
                break;
            case 'User':
                $('#jeb-Debug').text(JSON.stringify(User));
                break;
        }
    },
    dbgLog : "",
    dbgEnable : {},
    setHeader : function(txt) {
        $("#jeb-header").text(txt);
    },
    placePanels : function() {
        var devw = window.screen.width;
        var devh = window.screen.height;
        User.Screen = devw + 'x' + devh;

        var avw = window.screen.availWidth;
        var avh = window.screen.availHeight;
        User.AvailScreen = avw + 'x' + avh;

        var ow = window.outerWidth;
        var oh = window.outerHeight;
        User.Outer = ow + 'x' + oh;

        var w = window.innerWidth;
        var h = window.innerHeight;
        User.Inner = w + 'x' + h;

        var docw = document.body.clientWidth;
        var doch = document.body.clientHeight;
        User.Doc = docw + 'x' + doch;

        User.Agent = navigator.userAgent;
        User.Browser = navigator.appName;
        User.OS = navigator.platform;

        // IH = iphone header
        // AH = android notification bar
        // BA = browser address bar
        // BF = browser footer
        // HP = home page link
        // wxh = reported window.innerWidth x window.innerHeight
        // wXXh = reported window.outerWidth x window.outerHeight
        // iPhone 3G from home page: (header overlaps top line) p(320x396) l(320x396) P 320x480  L  320x214
        // iPhone 3G from Safari:  P 320x356  L 320x139    Rd 8
        // iPhone 4 from home page: (header overlaps top line) P iw=320 ih=480  L  iw=480 ih=320
        // Note/Android HP: AH+BA  P 400x537 t400  L 640x297 t304
        // Note/Dolphin HP: P 400x640 t-xtra  L 640x400  t-xtra
        // Note/Ffox HP: AH+BA P 400x567 t-xtra  L 640x327 t400 (bottom line half under footer)
        // Note/Opera HP: AH+BA+BF  P 356x393 t163 L 569x247
        // Evo/Android NAV (menu hdrs off & scrolled): P 369x615 L 615x369
        // Evo/XScope NAV (menu hdrs off): AH P 320x533 t139  L 533x320 t180

        var screen = (h < w) ? "Landscape" : "Portrait";
        if (window.device) {
            var dev = window.device;
            JEB.dbg('resize', "w.dev: {0} on {1} {2}w x {3}h ".format(dev.name, dev.platform, w, h));
        }
        JEB.dbg('resize', "{0} {1}w x {2}h mapped to {3}w x {4}h ".format(screen, avw, avh, w, h));
        var canv = Math.floor(h < w ? h * .8 : w * .8) + 'px';
        // @formatter:off
        if (screen == "Portrait") {
            $("#jeb-LoadMsg").css( { position: "absolute", top: "5%",  left: "10%", width: "70%", } );
            $("#jeb-Debug").css(    { position: "absolute", top: "15%", left: "10%", width: "70%", } );
			$("#jeb-Canvas").width(canv);
			$("#jeb-Canvas").height(canv);
            $('#ClockMeter').width(canv);
            $("#Clock").css(  { position: "absolute", top: "2em",  left: "3%",  width: canv, height: canv });
            $("#jeb-Table").css( { position: "absolute", top: "25%", left: "30%", width: "50%", height: "50%" });
        }
        else {
            $("#jeb-LoadMsg").css( { position: "absolute", top: "5%",  left: "10%", width: "70%", } );
            $("#jeb-Debug").css(    { position: "absolute", top: "10%", left: "10%", width: "40%", } );
			$("#jeb-Canvas").width(canv);
			$("#jeb-Canvas").height(canv);
			$('#ClockMeter').width(canv);
            $("#Clock").css(  { position: "absolute", top: "2em",  left: "3%",  width: canv, height: canv });
            $("#jeb-Table").css( { position: "absolute", top: "55%", left: "30%", width: "40%", height: "50%" });
         }
        // @formatter:on
    },
    placeClock : function(id, tp, lft, sz) {
        id = '#' + id;
        $(id + "-canvas").width(sz);
        $(id + "-canvas").height(sz);
        $(id + "-meter").width(sz - 6);
        var ht = $(id + "-meter").outerHeight(true) + sz + 1;
        // @formatter:off
        $(id + "-panel").css({ position : "absolute", top : tp+'px', left : lft+'px', width : sz+'px', height : ht+'px' });
        // @formatter:on
    },
    clockHtml : function(id) {
        var h = '<div id="{0}-clock" class="jebPanel jeb-Clock"><canvas id="{0}-canvas" class="jebCanvas"></canvas>'.format(id);
        h += '<div id="{0}-meter" class="jebMeter"><div id="{0}-weight" style="width: 75%"></div></div></div>'.format(id);
        return h;
    },
    addPanel : function(panelID, html) {
        var hd = '<div id="{0}-panel" class="jebPanel"> {1} </div>';
        hd = hd.format(panelID, html);
        $(hd).appendTo('body');
        JEB.panelHide(panelID + '-panel')
    },
    panelOpen : function(id) {
        $('#' + id + '-panel').fadeIn('slow');
    },
    panelClose : function(id) {
        $('#' + id + '-panel').fadeOut('slow');
    },
    panelHide : function(id) {
        $('#' + id + '-panel').hide();
    },
    menuOpen : function(id, body) {
        JEB.lastMenuID = id;
        var mid = '#' + id + '-menu';
        // $(mid).show('slow');
        $(mid).fadeIn('slow');
        JEB.onTouchClick(mid, 'a', JEB.menuClick);
        if (body) {
            JEB.menuBody = body;
            JEB.menuSelection = "";
        }
    },
    menuClose : function(id) {
        id = id || JEB.lastMenuID;
        var mid = '#' + id + '-menu';
        $(mid).fadeOut('slow');
    },
    menuHide : function(id) {
        id = id || JEB.lastMenuID;
        var mid = '#' + id + '-menu';
        $(mid).hide();
    },
    menuClick : function(target, event) {
        JEB.menuClose();
        var nm = $(target).text();
        JEB.menuSelection = JEB.menuSelection + ':' + nm;
        if ($(target).hasClass('jebSubMenu')) {
            JEB.menuOpen(nm);
        } else {
            var sel = JEB.menuSelection;
            JEB.menuSelection = "";
            JEB.lastMenuID = "";
            if (JEB.menuBody != null)
                JEB.menuBody(sel, target);
            else
                window.alert('menu ' + JEB.menuSelection + ' no body');
        }
    },
    rowOrder : function(count, columns) {
        // return array of 'count' indices as rows of 'columns',
        // where if columns < 0 ? fill columns left to right : fill rows top to bottom
        // e.g.  rowOrder(10,3) = [ [0, 1, 2], [3, 4, 5], [6, 7, 8], [9] ]
        // and   rowOrder(10,-3)= [ [0, 4, 7], [1, 5, 8], [2, 6, 9], [3] ]
        var ncols = columns > 0 ? columns : -columns;
        var nrows = Math.floor(count / ncols);
        if (nrows * ncols < count)
            nrows++;
        var rO = [];
        for (var i = 0; i < nrows; i++)
            rO.push([]);
        for (var r = 0; r < nrows; r++)
            for (var c = 0; c < ncols; c++) {
                var idx = columns > 0 ? (r * ncols + c) : (c * nrows + r);
                if (idx < count)
                    rO[r].push(idx);
            }
        return rO;
    },
    genItem : function(menuID, menu) {
        if ($.isArray(menu)) {//submenu
            var nm = menu[0].toString().trim();
            var html = ' <a class="jebSubMenu">{0}</a> '.format(nm);
            this.buildMenus(nm, menu[1], menu[2]);
            return html;
        } else {
            var nm = menu.toString().trim();
            return ' <a>{0}</a> '.format(nm);
        }
    },
    genRow : function(menuID, menus, itemOrder) {
        var html = "<div> ";
        for (var i = 0; i < itemOrder.length; i++)
            html += this.genItem(menuID, menus[itemOrder[i]]);
        return html + " </div>";
    },
    buildMenus : function(menuID, columns, menus) {
        if ( typeof menus == 'string')
            menus = menus.split(';');
        var rO = this.rowOrder(menus.length, columns);
        var html = '';
        for (var r = 0; r < rO.length; r++)
            html += this.genRow(menuID, menus, rO[r]);
        var menuHtml = '<div id="{0}" class="jebMenu jebButton"> {1} </div>'.format(menuID + '-menu', html);
        $(menuHtml).appendTo('body');
        this.menuHide(menuID);
    },
    splitPanel : function(pnl, horiz, minpx, maxpx, evenCnt, midpx) {
        // return array of css records that cover 'pnl', with 'horiz' stripes of 'minpx' on the left, 'maxpx' on the rights,
        //   and the rest in either 1) 'evenCnt' equal slices, or if evenCnt==0, 2) slices of 'midpx' with padding
        // defaults: horiz=false, minpx=0, maxpx=0, evenCnt=1
        var slices = [];
        var iL = 'left', iW = 'wide';
        if (!horiz) {
            iL = 'top';
            iW = 'high';
        }
        var left = pnl[iL], wide = pnl[iW];
        if (minpx && minpx > 0)
            slices.push(copyPnl(pnl, iW, minpx));
        //TODO: rest

        return slices;
    },
    copyPnl : function(pnl, i1, v1, i2, v2) {
        var p = {};
        for (var i in pnl) {
            p[i] = pnl[i];
            if (i == i1)
                p[i] = v1;
            if (i == i2)
                p[i] = v2;
        }
        return p;
    },
    onTouchClick : function(sel, subsel, body) {
        $(sel).on('touchstart click', subsel, function(event) {
            event.stopPropagation();
            event.preventDefault();
            if (event.handled != true) {
                body(this, event);
                event.handled = true;
            } else {
                return false;
            }
        });
    },
    genTableHtml : function(obj) {
        var html = "<table>";
        for (var i in obj) {
            html += "<tr><td>" + i + ":</td><td>";
            if (obj[i] instanceof String)
                html += obj[i];
            else if (obj[i] instanceof Object)
                html += JEB.genTableHtml(obj[i]);
            else
                html += obj[i].toString();
            html += "</td></tr>";
        }
        return html + "</table>";
    },
    onDeviceReady : function() {
        JEB.dbg("devReady", "window.device={0}".format(window.device));
        var phoneGapEvents = ["menubutton", "backbutton", "searchbutton", "pause", "resume", "online", "offline", "batterycritical", "batterylow", "batterystatus", "startcallbutton", "endcallbutton", "volumedownbutton", "volumeupbutton"];
        for (var i in phoneGapEvents)
        $(document).on(phoneGapEvents[i], JEB.onPGEvent);
        if (navigator && navigator.network)
            navigator.network.isReachable("google.com", JEB.reachableCallback, {});
    },
    onPGEvent : function(ev) {
        JEB.dbg("PGEvent", ev.type);
    },
    getGeo : function() {
        if (!navigator || !navigator.geolocation)
            return JEB.dbg("geo", "no navigator.geolocation");
        navigator.geolocation.getCurrentPosition(JEB.onGeoSuccess, JEB.onGeoError);
    },
    onGeoSuccess : function(position) {
        JEB.dbg("geo", 'Lat:{0} Lng:{1} Alt:{2} Acc:{3} Hd:{4} Spd: {5}'.format(position.coords.latitude, position.coords.longitude, position.coords.altitude, position.coords.accuracy, position.coords.heading, position.coords.speed, position.timestamp));
    },
    onGeoError : function(error) {
        JEB.dbg("geo", "Err: {0} {1}".format(error.code, error.message));
    },

    reachableCallback : function(reachability) {
        // There is no consistency on the format of reachability
        var networkState = reachability.code || reachability;
        var states = {};
        states[NetworkStatus.NOT_REACHABLE] = 'No network connection';
        states[NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK] = 'Carrier data connection';
        states[NetworkStatus.REACHABLE_VIA_WIFI_NETWORK] = 'WiFi connection';
        if (networkState != 0)
            JEB.online = true;
    },
    online : navigator.onLine || false,
    refreshData : function(key) {
        //now if you about to make an AJAX call to load up some dynamic data, you can easily check to see if you're online
        if (JEB.online) {
            // make an AJAX request
        } else {
            // load from localStorage
        }
    },

    styles : {
        red : 'fillcolor:#FF0000;fill:true;',
        green : 'fillcolor:#00E000;fill:true;',
        blue : 'fillcolor:#0000E0;fill:true;',
        yellow : 'fillcolor:#E0E000;fill:true;',
        white : 'fillcolor:#FFFFFF;fill:true;',
        black : 'fillcolor:#000000;fill:true;',
        randFill : 'fillcolor:RndC();fill:true;',
        redEdge : 'lineColor:#FF0000;draw:true;',
        greenEdge : 'lineColor:#00E000;draw:true;',
        blueEdge : 'lineColor:#0000E0;draw:true;',
        yellowEdge : 'lineColor:#E0E000;draw:true;',
        whiteEdge : 'lineColor:#FFFFFF;draw:true;',
        blackEdge : 'lineColor:#000000;draw:true;',
        randEdge : 'lineColor:RndC();draw:true;',
        redInBlack : 'red;blackEdge;',
        blueInBlack : 'blue;blackEdge;',
        greenInWhite : 'green;whiteEdge;',
        randInBlack : 'randFill;blackEdge;',
        randInWhite : 'randFill;whiteEdge;',
    },
    addStyle : function(nm, sty) {
        JEB.styles[nm] = sty;
    },
    setOptions : function(obj, options, styles) {
        return this.setObjFields(obj, options, styles, true);
    },
    setObjFields : function(obj, options, styles, doOptFuncs) {
        if (options == null)
            return obj;
        var opts = options.split(';');
        for (var i in opts) {// "name:val; name:true; name:false; name;"
            var op = opts[i].trim();
            if (op == '')
                continue;
            var nm, val;
            var icolon = op.indexOf(':');
            if (icolon >= 0) {
                nm = op.substr(0, icolon);
                val = op.substr(icolon + 1);
            } else {// no ':' indicates a style
                nm = op;
                val = 'style';
            }
            if (val == 'style') {
                if (doOptFuncs && typeof nm == 'string' && nm.indexOf('(') > 0)// allow RndOpt(x,y,z,styles)
                    nm = this.optFunc(nm);
                if (styles && styles[nm])
                    this.setObjFields(obj, styles[nm], styles, doOptFuncs);
                else if (JEB.styles[nm])
                    this.setObjFields(obj, JEB.styles[nm], styles, doOptFuncs);
            }
            if (val == 'true')
                val = true;
            if (val == 'false')
                val = false;
            if (doOptFuncs && typeof val == 'string' && val.indexOf('(') > 0)
                val = this.optFunc(val);
            if ( typeof val == 'string' && !isNaN(Number(val)))
                val = Number(val);
            if ( typeof val == 'string' && val.indexOf(',') > 0)
                val = val.split(',');
            var dots = nm.split('.');
            var sobj = obj;
            for (var i = 0; i < dots.length - 1; i++) {
                if (dots[i].indexOf('[') == 0) {// array: '[0]'
                    dots[i] = Number(dots[i].substring(1, dots[i].length - 1));
                    if (!sobj[dots[i]])
                        sobj = [];
                    // create array
                } else {
                    if (!sobj[dots[i]])
                        sobj[dots[i]] = {};
                    // create sub obj
                }
                sobj = sobj[dots[i]];
            }
            sobj[dots[dots.length - 1]] = val;
        }
        return obj;
    },
    varTrans : {},
    cdTrans : {},
    varCnt : 0,
    varSyms : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
    varRef : function(obj, fieldnm, dS) {
        if (this.varTrans[fieldnm] == undefined) {
            this.varTrans[fieldnm] = this.varSyms.charAt(this.varCnt);
            this.cdTrans[this.varSyms.charAt(this.varCnt++)] = fieldnm;
        }
        var f = obj[fieldnm];
        if (!dS)
            return f;

        if ( typeof f == 'string' && f.charAt(0) == '{') {
            return this.evalVar(f.substring(1, f.indexOf('}')), dS);
        } else
            return f;
    },
    evalVar : function(expr, dS) {
        var isty = expr.indexOf('|');
        var fmt = isty < 0 ? 'v' : expr.substr(isty + 1);
        var nm = isty < 0 ? expr : expr.substring(0, isty);
        var val;
        switch (fmt.charAt(0)) {
            case 'v':
            case 'c':
            default:
                val = dS.toValue(nm);
                break;
            case 'f':
                val = dS.toFraction(nm);
                break;
            case 's':
                val = dS.toString(nm);
                break;
        }
        if (val == undefined)
            val = '?undef';
        if (fmt.length > 1) {
            var ln = Number(fmt.substr(1));
            if ( typeof val == 'string' && val.length > ln)
                val = val.substr(0, ln);
            else
                val = JEB.pad2int(val, ln);
        }
        if (fmt.charAt(0) == 'c')
            val = ':' + val;
        return val;
    },
    substVars : function(s, dS) {
        // replace {x}, {x|f}, {x|s} with evalVar(x,dS)
        var bv = s.indexOf('{');
        var ev = s.indexOf('}');
        while (bv >= 0 && ev > bv) {
            var expr = s.substring(bv + 1, ev);
            var val = this.evalVar(expr, dS);

            s = s.substr(0, bv) + val + s.substr(ev + 1);
            bv = s.indexOf('{');
            ev = s.indexOf('}');
        }
        return s;
    },
    optsToObj : function(options, doOptFunctions) {
        return this.setObjFields({}, options, doOptFunctions);
    },
    objToOpts : function(obj) {
        return this.subobjToOpts('', obj);
    },
    subobjToOpts : function(pref, obj) {
        var s = '';
        if (!obj.fields) {
            if ( typeof obj == 'object' && obj instanceof Array) {
                for (var i = 0; i < obj.length; i++) {
                    s += this.subobjToOpts(pref + '[' + i + '].', obj[i]);
                }
                return s;
            }
            return s;
        }
        for (var i in obj) {
            if (obj.fields == '*' || obj.fields.indexOf(i) >= 0) {
                if ( typeof obj[i] == 'object')
                    s += this.subobjToOpts(i + '.', obj[i]);
                else
                    s += '{0}{1}:{2};'.format(pref, i, obj[i]);
            }
        }
        //        var fields = obj.fields.split(';');
        //        for (var i in fields) {
        //            if (typeof obj[fields[i]] == 'object')
        //                s += this.subobjToOpts(i + '.', obj[fields[i]]);
        //            else
        //                s += '{0}{1}:{2};'.format(pref, fields[i], obj[fields[i]]);
        //        }
        return s;
    },
    degToRad : Math.PI / 180,
    pctToRad : 2 * Math.PI / 100,
    optFunc : function(optval) {
        if ( typeof optval != 'string')
            return optval;
        var lpar = optval.indexOf('(');
        var val = optval;
        if (lpar >= 0) {
            var parms = optval.substring(lpar + 1, optval.length - 1).split(',');
            var asInt = false;
            var fn = optval.substring(0, lpar);
            switch (fn) {
                case 'RndOpt':
                    val = parms[Math.floor(Math.random() * parms.length)];
                    break;
                case 'RndC':
                    var h = this.rndRng(parms[0], 0, 360, false) / 360;
                    var s = this.rndRng(parms[1], 0, 1, false);
                    var l = this.rndRng(parms[2], 0, 1, false);
                    val = this.hslToWeb(h, s, l);
                    break;
                case 'RndI':
                    asInt = true;
                // fall through
                case 'Rnd':
                    val = this.rndRng(parms[0], 0, 100, asInt);
            }
        }
        return val;
    },
    rndRng : function(parm, defmin, defmax, asInt) {// parm is 'max' or 'min..max'
        var min = defmin;
        var max = defmax;
        if (parm && parm.trim() != '') {
            var sep = parm.indexOf('..');
            if (sep >= 0) {
                min = Number(parm.substring(0, sep));
                max = Number(parm.substring(sep + 2, parm.length));
            } else
                max = Number(parm);
        }
        var rnd = Math.random() * (max - min) + min;
        if (asInt)
            rnd = Math.floor(rnd);
        return rnd;
    },
    webToHSL : function(color) {//color '#rrggbb'
        var r = parseInt(color.substr(1, 2), 16);
        // Grab the hex representation of red (chars 1-2) and convert to decimal (base 10).
        var g = parseInt(color.substr(3, 2), 16);
        var b = parseInt(color.substr(5, 2), 16);
        return JEB.rgbToHsl(r, g, b);
    },
    pad2hex : function(v) {
        v = Math.round(v);
        if (v > 255)
            v = 255;
        var s = v.toString(16);
        if (s.length == 1)
            s = '0' + s;
        return s;
    },
    pad2int : function(v, padlength) {
        return this.pad(v, padlength || 2, '00000000000');
    },
    pad2field : function(v, fieldwidth) {
        return this.pad(v, fieldwidth || 8, '              ');
    },
    pad : function(v, desiredlen, padstring) {
        var len = desiredlen || 2;
        var padding = padstring || '000000';
        var s = v.toString();
        if (s.length < len)
            s = padding.substr(0, len - s.length) + s;
        return s;
    },
    hslToWeb : function(h, s, l) {
        /**
         * Converts an RGB color value to HSL. Conversion formula
         * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
         * Assumes r, g, and b are contained in the set [0, 255] and
         * returns h, s, and l in the set [0, 1].
         *
         * @param   Number  r       The red color value
         * @param   Number  g       The green color value
         * @param   Number  b       The blue color value
         * @return  Array           The HSL representation
         */
        var rgb = this.hslToRgb(h, s, l);
        return '#' + this.pad2hex(rgb[0]) + this.pad2hex(rgb[1]) + this.pad2hex(rgb[2]);
    },
    rgbToHsl : function(r, g, b) {
        r /= 255, g /= 255, b /= 255;
        var max = Math.max(r, g, b), min = Math.min(r, g, b);
        var h, s, l = (max + min) / 2;

        if (max == min) {
            h = s = 0;
            // achromatic
        } else {
            var d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
            }
            h /= 6;
        }

        return [h, s, l];
    },
    hslToRgb : function(h, s, l) {
        /**
         * Converts an HSL color value to RGB. Conversion formula
         * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
         * Assumes h, s, and l are contained in the set [0, 1] and
         * returns r, g, and b in the set [0, 255].
         *
         * @param   Number  h       The hue
         * @param   Number  s       The saturation
         * @param   Number  l       The lightness
         * @return  Array           The RGB representation
         */
        var r, g, b;
        if (s == 0) {
            r = g = b = l;
            // achromatic
        } else {
            function hue2rgb(p, q, t) {
                if (t < 0)
                    t += 1;
                if (t > 1)
                    t -= 1;
                if (t < 1 / 6)
                    return p + (q - p) * 6 * t;
                if (t < 1 / 2)
                    return q;
                if (t < 2 / 3)
                    return p + (q - p) * (2 / 3 - t) * 6;
                return p;
            }

            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = hue2rgb(p, q, h + 1 / 3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1 / 3);
        }
        return [r * 255, g * 255, b * 255];
    }
}
JEB.Version = "22OctH";

var User = {};
var DeviceDescriptor = {
    Class : ["Where are you reading this page?", "PC", "tablet", "phone"],
    OS : {
        'PC' : ["Which PC OS?", "Windows", "MacOS", "Linux", "other"],
        'tablet' : ["Which tablet OS?", "iOS", "Android", "Kindle", "Nook", "other"],
        'phone' : ["Which phone OS?", "iOS", "Android", "WinPhone", "Blackberry", "other"]
    },
    Browser : {
        'PC' : ["Which PC browser?", "IE", "Chrome", "Safari", "Firefox", "Opera", "other"],
        'iOS' : ["Which iOS browser?", "Safari", "Chrome", "Dolphin", "Opera", "other"],
        'Android' : ["Which Android browser?", "Android", "Dolphin", "Safari", "Firefox", "Opera", "Chrome", "XScope", "other"]
    },
    Screen : {
        'PC' : ["How big is your screen?", "480x640", "768x1024", "720x1280", '1080x1920', "other"],
        'iOS' : ["How big is your screen?", "320x480", "640x960", "768x1024", "1536x2048"],
        'Android' : ["How big is your screen?", "240x320", "320x480", "480x800", "540x960", "720x1280", "768x1024", "800x1280"],
    }
};
JEB.filler = function(proto, htmlID, after) {
    JEB.currFiller = this;
    this.fProto = proto;
    this.fHtmlID = htmlID;
    this.fResult = {};
    this.fField = this.emptyIndex();
    this.after = after;
    var html = this.genHtml(this.fField);
    var id = '#' + this.fHtmlID;
    $(id).html(html);
    JEB.onTouchClick(id, 'a', function(targ, event) {
        var f = JEB.currFiller;
        f.fResult[f.fField] = $(targ).text();
        f.fField = f.emptyIndex();
        if (f.fField == null) {
            $('#' + f.fHtmlID).html(f.genTable());
            $('#' + f.fHtmlID).off('touchstart click');
            f.after(f.fResult);
        } else {
            var html = f.genHtml(f.fField);
            var id = '#' + f.fHtmlID;
            $(id).html(html);
        }
    });
};
JEB.filler.prototype = {
    emptyIndex : function() {
        for (var i in this.fProto)
        if (this.fResult[i] == undefined) {
            return i;
        }
        return null;
    },
    genHtml : function(fld) {
        var fieldChoices = this.fProto[fld];
        if ( fieldChoices instanceof Object) {
            var asJson = JSON.stringify(this.fResult);
            for (var j in fieldChoices) {
                if (asJson.indexOf(j) >= 0) {
                    fieldChoices = fieldChoices[j];
                    break;
                }
            }
        }
        var html = fieldChoices[0];
        for (var j = 1; j < fieldChoices.length; j++)
            html += "<br><a>" + fieldChoices[j] + "<a>";
        return html;
    },
    genTable : function() {
        var html = "<table>";
        for (var i in this.fResult)
        html += "<tr><td>" + i + ":</td><td>" + this.fResult[i].toString() + "</td></tr>";
        return html + "</table>";
    }
}

JEB.drawing = function(canvas, options) {
    this.name = canvas.id;
    this.canvas = canvas;
    this.width = canvas.width;
    this.height = canvas.height;
    this.radius = Math.min(this.width, this.height) / 2.0;
    this.cx = this.width / 2.0;
    this.cy = this.height / 2.0;
    this.ctx = canvas.getContext('2d');
    return this.init(options);
};
JEB.drawing.prototype = {
    styles : {},
    objs : {},
    refreshtics : 1000,
    init : function(options) {
        this.alpha = 1.0;
        this.fillcolor = '#8080FF';
        this.linecolor = '#000000';
        this.children = [];
        this.options = options;
        JEB.setOptions(this, options, this.styles);
        return this;
    },
    vRef : function(obj, fieldname) {
        return JEB.varRef(obj, fieldname, this.dataSource);
    },
    render : function() {
        if (!this.ctx || document.getElementById(this.name) == null)
            return;
        this.ctx.clearRect(0, 0, this.width, this.height);
        // to transparent 0's
        this.ctx.globalAlpha = this.vRef(this, 'alpha') / 100;
        this.ctx.fillStyle = this.vRef(this, 'fillcolor');
        this.ctx.fillRect(0, 0, this.width, this.height);
        for (var i in this.children) {
            this.children[i].render(this.ctx);
        }
        return this;
    },
    refresh : function(self) {
        if (self.refreshtics < 0)
            return;
        if (self.dataSource && self.dataSource.update)
            self.dataSource.update();
        self.render();
        window.setTimeout(function() {
            JEB.drawing.prototype.refresh(self)
        }, self.refreshtics);
    },
    erase : function() {
        this.children = [];
    },
    pctToX : function(x) {// maps 0..100 into canvas x
        return this.width * x / 100;
    },
    pctToY : function(y) {// maps 100..0 into canvas y
        return this.height * (100 - y) / 100;
    },
    pctToSz : function(d) {//maps 0..100 into canvas min dimension
        return Math.min(this.width, this.height) * d / 100;
    },
    pctToCrds : function(x, y, d1, d2) {
        return {
            x : this.pctToX(x),
            y : this.pctToY(y),
            d1 : this.pctToSz(d1),
            d2 : this.pctToSz(d2)
        };
    }
}

JEB.rect = function(drawing, par, options) {
    this.drawing = drawing;
    this.parent = par;
    par.children.push(this);
    return this.init(options);
}
JEB.rect.cnt = 0;
JEB.rect.styles = {};
JEB.rect.prototype = {
    init : function(options) {// all units in %
        this.name = 'rect' + JEB.rect.cnt++;
        this.drawing.objs[this.name] = this;
        this.wide = 10;
        this.high = 10;
        this.lx = 50;
        this.ty = 50;
        this.rotate = 0;
        this.angleoffset = 75;
        // good for clocks!
        this.cx = 50;
        this.cy = 50;
        this.draw = true;
        this.linecolor = "#000000";
        this.linewidth = 2;
        this.fill = false;
        this.alpha = 1.0;
        this.fillcolor = "#ff0000";
        this.options = options;
        JEB.setOptions(this, options, JEB.rect.styles);
        return this;
    },
    vRef : function(fieldname) {
        if (this.drawing)
            return this.drawing.vRef(this, fieldname);
        else
            return this[fieldname];
    },
    render : function(ctx) {
        if (this.vRef('hidden'))
            return;
        ctx.save();
        var crds = this.drawing.pctToCrds(this.vRef('lx'), this.vRef('ty'), this.vRef('wide'), this.vRef('high'));
        //        if (this.vRef('rotate') != 0) {
        var cx = this.drawing.pctToX(this.vRef('cx'));
        var cy = this.drawing.pctToY(this.vRef('cy'));
        ctx.translate(cx, cy);
        var ang = this.vRef('rotate') + this.vRef('angleoffset');
        ctx.rotate(ang * JEB.pctToRad);
        crds.x -= cx, crds.y -= cy;
        //        }
        ctx.fillStyle = this.vRef('fillcolor');
        ctx.globalAlpha = this.vRef('alpha');
        if (this.vRef('fill'))
            ctx.fillRect(crds.x, crds.y, crds.d1, crds.d2);

        ctx.strokeStyle = this.vRef('linecolor');
        ctx.lineWidth = this.vRef('linewidth');
        if (this.vRef('draw'))
            ctx.strokeRect(crds.x, crds.y, crds.d1, crds.d2);

        var txt = this.vRef('text');
        if (txt) {
            ctx.textBaseline = "top";
            ctx.font = crds.d2 * .6 + "pt Arial";
            ctx.fillStyle = this.vRef('linecolor');
            ctx.fillText(txt, crds.x + 2, crds.y);
        }
        ctx.restore();
        return this;
    }
};

JEB.marker = function(drawing, par, options) {
    this.drawing = drawing;
    this.parent = par;
    par.children.push(this);
    return this.init(options);
}
JEB.marker.cnt = 0;
JEB.marker.styles = {};
JEB.marker.prototype = {
    init : function(options) {
        this.name = 'marker' + JEB.marker.cnt++;
        this.drawing.objs[this.name] = this;
        this.draw = true;
        this.linecolor = "#58b0AA";
        this.fill = false;
        this.alpha = 1.0;
        this.fillcolor = "#A725D3";
        this.cx = this.parent.cx;
        this.cy = this.parent.cy;
        this.irad = 5;
        this.orad = 10;
        this.npts = 5;
        this.nloops = 3;
        this.options = options;
        JEB.setOptions(this, options, JEB.marker.styles);
        this.step = 360 * this.nloops / this.npts;
        return this;
    },
    vRef : function(fieldname) {
        if (this.drawing)
            return this.drawing.vRef(this, fieldname);
        else
            return this[fieldname];
    },
    render : function(ctx) {
        if (this.vRef('hidden'))
            return;
        ctx.save();
        ctx.beginPath();
        var cx = this.vRef('cx');
        var cy = this.vRef('cy');
        var irad = this.vRef('irad');
        var orad = this.vRef('orad');
        var npts = this.vRef('npts');
        var step = this.vRef('step') * JEB.degToRad;
        var crds = this.drawing.pctToCrds(cx, cy, irad, orad);
        ctx.moveTo(crds.x + crds.d2, crds.y);
        var a = step;
        for (var i = 0; i < npts; i++) {
            var rad = (i % 2 == 0) ? crds.d1 : crds.d2;
            var ax = crds.x + rad * Math.cos(a);
            var ay = crds.y + rad * Math.sin(a);
            ctx.lineTo(ax, ay);
            a += step;
        }
        ctx.closePath();
        ctx.strokeStyle = this.vRef('linecolor');
        ctx.fillStyle = this.vRef('fillcolor');
        ctx.globalAlpha = this.vRef('alpha');
        if (this.vRef('draw'))
            ctx.stroke();
        if (this.vRef('fill'))
            ctx.fill();
        ctx.restore();
    }
}

JEB.scale = function(drawing, par, options) {
    this.drawing = drawing;
    this.parent = par;
    par.children.push(this);
    return this.init(options);
}
JEB.scale.cnt = 0;
JEB.scale.styles = {
    vert0to100by10 : 'style:vertical;mintic:0;maxtic:100;ticstep:10;ticalign:center;ticstart:3;ticlen:5;drawbase:true;',
    vert0to100by10at10to90 : 'style:vertical;mintic:0;maxtic:100;ticstep:10;ticalign:center;ticstart:3;ticlen:5;minval:10;maxval:90,drawbase:true;',
    vert10to90by5 : 'style:vertical;mintic:10;maxtic:90;ticstep:5;ticalign:center;ticstart:3;ticlen:5;drawbase:true;',
    horiz0to100by10 : 'style:horizontal;mintic:0;maxtic:100;ticstep:10;ticalign:center;ticstart:3;ticlen:5;drawbase:true;',
    horiz10to90by5 : 'style:horizontal;mintic:10;maxtic:90;ticstep:5;ticalign:center;ticstart:3;ticlen:5;drawbase:true;',
    clock0to100by10 : 'style:clockwise;mintic:0;maxtic:100;ticstep:10;ticalign:center;ticstart:3;ticlen:5;drawbase:true;',
    clock10to90by5 : 'style:clockwise;mintic:10;maxtic:90;ticstep:5;ticalign:center;ticstart:3;ticlen:5;drawbase:true;',
    ccw0to100by10 : 'style:counter-cw;mintic:0;maxtic:100;ticstep:10;ticalign:center;ticstart:3;ticlen:5;drawbase:true;',
    ccw10to90by5 : 'style:counter-cw;mintic:10;maxtic:90;ticstep:5;ticalign:center;ticstart:3;ticlen:5;drawbase:true;',
};
JEB.scale.prototype = {
    init : function(options) {//'radius:100;cx:50;cy:50;style:vertical;mintic:0;maxtic:100;ticstep:10;ticstart:0;ticlen:5;ticwidth:1;ticalign:center;draw:false;lineColor:#0000FF;fill:true;fillcolor:#1010E0;'
        this.name = 'scale' + JEB.scale.cnt++;
        this.drawing.objs[this.name] = this;
        this.minvalue = 0;
        this.maxvalue = 100;
        // scale area is centered at cx,cy with given radius
        this.radius = 100;
        this.angleoffset = 75;
        // good for clocks!
        // 0 at top
        this.cx = 50;
        this.cy = 50;
        this.style = "vertical";
        // horizontal,clockwise,counter-cw
        // tics go from mintic..maxtic in steps of ticstep
        this.mintic = 0;
        this.maxtic = 100;
        this.ticstep = 10;
        // % to next tic
        // each tic begins at 'ticstart' and goes for 'ticlen' with thickness 'ticwidth'
        this.ticstart = 0;
        // % from left,bottom,center
        this.ticlen = 5;
        this.ticwidth = 1;
        // of each tic, in % value
        this.ticalign = 'center';
        this.draw = true;
        this.lineColor = "#0000FF";
        this.fill = true;
        this.fillcolor = "#1010E0";
        this.options = options;
        JEB.setOptions(this, options, JEB.scale.styles);
        return this;
    },
    vRef : function(fieldname) {
        if (this.drawing)
            return this.drawing.vRef(this, fieldname);
        else
            return this[fieldname];
    },
    scaleVal : function(val) {//TODO: sort out ticrange, scaleval
        var minV = this.vRef('minvalue'), maxV = this.vRef('maxvalue');
        var mintic = this.vRef('mintic'), ticR = this.vRef('maxtic') - mintic;
        var vnorm = (val - minV) / (maxV - minV);
        // 0..1
        return mintic + vnorm * ticR;
    },
    render : function(ctx) {
        if (this.vRef('hidden'))
            return;
        //for style = horizontal/vertical/clockwise/counter-cw
        //scale goes from mintic..maxtic  (as % of width/height/circumference)
        //base is at ticstart-ticwidth..ticstart X mintic-ticwidth/2..maxtic+ticwidth/2
        //a tic goes ticstart..ticstart+ticlen   X scale(mintic+N*ticstep)-ticwidth/2..scale(mintic+N*ticstep)+ticwidth/2
        ctx.save();
        ctx.globalAlpha = this.vRef('alpha');
        ctx.strokeStyle = this.vRef('linecolor');
        ctx.fillStyle = this.vRef('fillcolor');
        var ticS = this.vRef('ticstart'), ticW = this.vRef('ticwidth');
        var mint = this.vRef('mintic'), maxt = this.vRef('maxtic'), ticL = this.vRef('ticlen');
        var ticR = maxt - mint;
        var draw = this.vRef('draw'), fill = this.vRef('fill'), style = this.vRef('style');
        if (this.vRef('drawbase'))
            this.drawTic(ctx, ticS - ticW, ticW, mint - ticW / 2, ticR + ticW, draw, fill, style);
        var ticstep = this.vRef('ticstep');
        var val = this.vRef('minvalue');
        var maxv = this.vRef('maxvalue');
        while (val <= maxv) {
            var tic = this.scaleVal(val) - ticW / 2;
            this.drawTic(ctx, ticS, ticL, tic, ticW, draw, fill, style);
            val += ticstep;
        }
        ctx.restore();
    },
    drawTic : function(ctx, offset, len, val, thick, draw, fill, ticStyle) {
        // draw depending on style, a rectangle/arc that goes from val..val+thick X offset..offset+len
        //   val, thick are %(mintick..maxtick), offset & len are %(width/height/radius)
        //  depends on scale attributes 'style', 'cx','cy','radius', 'angleoffset', 'linecolor', 'fillcolor'
        var cx = this.vRef('cx'), cy = this.vRef('cy'), rad = this.vRef('radius');
        var aoff = this.vRef('angleoffset');
        var orad = (100 - offset) * rad / 100;
        var irad = (100 - offset - len) * rad / 100;
        ticStyle = ticStyle || this.vRef('style');
        switch (ticStyle) {
            case 'vertical':
                // offset->lx,val+thick->ty,len->wide,thick->high
                this.scRectangle(ctx, offset, val + thick, len, thick, draw, fill);
                break;
            case 'horizontal':
                // val->lx, offset+len->ty, thick->wide, len->high
                this.scRectangle(ctx, val, offset + len, thick, len, draw, fill);
                break;
            case 'clockwise':
                // val->start angle(*align), val+thick->end angle, offset->inner radius, offset+len->outer radius
                this.scArcBand(ctx, cx, cy, irad, orad, val + aoff, val + aoff + thick, false, draw, fill);
                break;
            case 'counter-cw':
                this.scArcBand(ctx, cx, cy, irad, orad, val + aoff, val + aoff + thick, true, draw, fill);
                break;
        }
    },
    scArcBand : function(ctx, cx, cy, irad, orad, ang1, ang2, ccw, draw, fill) {
        var crds = this.drawing.pctToCrds(cx, cy, irad / 2, orad / 2);
        ang1 *= JEB.pctToRad;
        ang2 *= JEB.pctToRad;
        ctx.beginPath();
        if (ccw) {
            ctx.arc(crds.x, crds.y, crds.d2, -ang1, -ang2, ccw);
            // outer arc from ang1 to ang2
            ctx.arc(crds.x, crds.y, crds.d1, -ang2, -ang1, !ccw);
        } else {
            ctx.arc(crds.x, crds.y, crds.d2, ang1, ang2, ccw);
            // outer arc from ang1 to ang2
            ctx.arc(crds.x, crds.y, crds.d1, ang2, ang1, !ccw);
        }
        ctx.closePath();
        if (draw)
            ctx.stroke();
        if (fill)
            ctx.fill();
    },
    scRectangle : function(ctx, x1, y1, w, h, draw, fill) {
        var crds = this.drawing.pctToCrds(x1, y1, w, h);
        if (draw)
            ctx.strokeRect(crds.x, crds.y, crds.d1, crds.d2);
        if (fill)
            ctx.fillRect(crds.x, crds.y, crds.d1, crds.d2);
    },
}

JEB.fraction = function(drawing, par, scale, options) {
    this.drawing = drawing;
    this.parent = par;
    this.scale = scale;
    par.children.push(this);
    return this.init(options);
}
JEB.fraction.styles = {};
JEB.fraction.cnt = 0;
JEB.fraction.prototype = {
    init : function(options) {
        this.name = 'fraction' + JEB.fraction.cnt++;
        this.drawing.objs[this.name] = this;
        this.draw = false;
        this.linecolor = "#ff0000";
        this.fill = true;
        this.alpha = 1.0;
        this.fillcolor = "#00ffff";
        this.cx = this.parent.cx;
        this.cy = this.parent.cy;
        this.ticwidth = 0;
        this.minvalue = this.scale.vRef('minvalue');
        this.options = options;
        JEB.setOptions(this, options, JEB.fraction.styles);
        return this;
    },
    vRef : function(fieldname) {
        if (this.drawing)
            return this.drawing.vRef(this, fieldname);
        else
            return this[fieldname];
    },
    render : function(ctx) {
        if (this.vRef('hidden'))
            return;
        ctx.save();
        ctx.globalAlpha = this.vRef('alpha');
        ctx.strokeStyle = this.vRef('linecolor');
        ctx.fillStyle = this.vRef('fillcolor');
        var thick = this.vRef('ticwidth');
        var val = this.scale.scaleVal(this.vRef('value'));
        var minval = this.scale.scaleVal(this.vRef('minvalue'));
        if (thick == 0)
            this.scale.drawTic(ctx, this.vRef('ticstart'), this.vRef('ticlen'), minval, val - minval, this.vRef('draw'), this.vRef('fill'));
        else
            this.scale.drawTic(ctx, this.vRef('ticstart'), this.vRef('ticlen'), val - thick / 2, thick, this.vRef('draw'), this.vRef('fill'));
        ctx.restore();
    },
}

JEB.timebase = function(options) {// options: "epoch:0;units:standard/grallt/swatch;"
    return this.init(options);
};
JEB.timebase.stat = {
    cnt : 0,
    styles : {},
    systems : {
        standard : {
            units : {
                second : 'min:0;max:59;',
                minute : 'min:0;max:59;',
                hour : 'min:0;max:11;',
                hour24 : 'min:0;max:23;strings:midnight,1am,2am,3am,4am,5am,6am,7am,8am,9am,10am,11am,noon,1pm,2pm,3pm,4pm,5pm,6pm,7pm,8pm,9pm,10pm,11pm;',
                day : 'min:1;max:31;',
                weekday : 'min:0;max:6;strings:Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday;',
                month : 'min:1;max:12;strings:January,February,March,April,May,June,July,August,September,October,November,December;',
                year : 'min:2000;max:2020;strings:2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020;',
            },
            formats : {
                standard : '{weekday|s2} {day}-{month|s3}-{year} {hour24|v2}:{minute|v2}:{second|v2}',
                usnumeric : '{month}/{day}/{year} {hour24}:{minute|v2}:{second|v2}',
                usalpha : '{weekday|s} {month|s} {day}, {year} {hour24}:{minute|v2}:{second|v2}',
                usshort : '{day}-{month|s3}-{year} {hour24}:{minute|v2}',
                usshortday : '{weekday|s3} {day}-{month|s3}-{year} {hour24}:{minute|v2}',
                uslong : '{weekday|s} {month|s} {day}, {year} {hour24}:{minute|v2}:{second|v2}',
                numeric : '{year}.{month|v2}.{day|v2}.{hour24|v2}.{minute|v2}',
            }
        },
        grallt : {
            units : {
                antle : 'msec:549.31640625;min:0;max:63;',
                tle : 'msec:35156.25;min:0;max:63;',
                utle : 'msec:2250000;min:0;max:63;',
                ande : 'msec:18000000;min:1;max:8;',
                llor : 'msec:108000000;min:1;max:64;',
                zul : 'msec:6912000000;min:0;max:5;',
            },
            formats : {
                standard : '{zul}/{llor}.{ande} {utle}:{tle}:{antle}',
            }
        },
        swatch : {
            units : {
                centibeat : 'msec:864;min:0;max:99;',
                beat : 'msec:86400;min:0;max:999;',
            },
            formats : {
                standard : '@{beat}.{centibeat|d2}',
            }
        },
    }
};
JEB.timebase.prototype = {
    init : function(options) {// options: 'name:, units:, epoch: '
        this.name = 'timebase' + JEB.timebase.stat.cnt++;
        // array of unit descriptors
        this.units = JEB.timebase.stat.systems['standard'].units;
        this.formats = JEB.timebase.stat.systems['standard'].formats;

        this.epoch = 0;
        this.options = options;
        JEB.setOptions(this, options, JEB.timebase.stat.styles);
        if ( typeof this.units == 'string') {// e.g. option "units:grallt;"
            if (this.units == 'swatch') {//TODO: adj epoch for swatch?
                var nw = new Date();
                // local timezone in minutes
                var tz = nw.getTimezoneOffset();
                // offset for UTC+1hr (BielMeanTime)
                this.epoch = (tz + 60) * 60000;
                //offset in msec
            }
            this.formats = JEB.timebase.stat.systems[this.units].formats;
            this.units = JEB.timebase.stat.systems[this.units].units;
        }
        // cvt to array of objects
        this.units = this.createUnits(this.units);

        // initialize dynamic variables
        // if 'dpct1' is defined, then {dpct1} will be updated each tic
        //    based on dpct1, dstp1=5, dmin1=0, dmax1=100, dwr1=0,
        // if 'dcolor1' is defined, {dcolor1} will be updated
        //    based on dcolor1, dhuestp1=5, dhuemin1=0, dhuemax1=100, dhuewr1=0,
        //    dsatstp1=5, dsatmin1=0, dsatmax1=0, dsatwr1=0,
        //    dbrtstp1=5, dbrtmin1=0, dbrtmax1=0, dbrtwr1=0
        this.dyna = {}
        var vals = "stp,min,max,wr".split(',');
        for (var i = 1; i < 50; i++) {
            if (this['dcolor' + i]) {
                var nm = 'color' + i;
                var clr = this['dcolor' + i];
                // @formatter:off
                this.dyna[nm] = { val: 0, dhue: { value: 0, stp: 5, min: 0, max: 100, wr: 1  },
                    dsat: {
                        value: 0,
                        stp: 5,
                        min: 0,
                        max: 100,
                        wr: 1
                    },
                    dbrt: {
                        value: 0,
                        stp: 5,
                        min: 0,
                        max: 100,
                        wr: 1
                    },
                };
                // @formatter:on
                var hsl = JEB.webToHSL(clr);
                this.dyna[nm].dhue.value = hsl[0] * 100;
                this.dyna[nm].dsat.value = hsl[1] * 100;
                this.dyna[nm].dbrt.value = hsl[2] * 100;
                var props = "dhue,dsat,dbrt".split(',');
                for (var p in props)
                for (var v in vals) {
                    var fld = props[p] + vals[v] + i;
                    if (this[fld] != undefined)
                        this.dyna[nm][props[p]][vals[v]] = this[fld];
                }
            }
            if (this['dpct' + i]) {
                var nm = 'pct' + i;
                // @formatter:off
                this.dyna[nm] = {
                    value: 0,
                    stp: 5,
                    min: 0,
                    max: 100,
                    wr: 1
                };
                // @formatter:on
                this.dyna[nm].value = this['dpct' + i];
                for (var v in vals) {
                    var fld = 'd' + vals[v] + i;
                    if (this[fld] != undefined)
                        this.dyna[nm][vals[v]] = this[fld];
                }
            }
        }
        var nw = new Date();
        this.update(nw);
        return this;
    },
    v : function(fieldname) {
        return this.drawing.varRef(this, fieldname, this.drawing.dataSource);
    },
    createUnits : function(optarray) {
        var un = {};
        for (var i in optarray)
        un[i] = JEB.optsToObj(optarray[i]);
        return un;
    },
    updateDyna : function(dy, diff) {
        var v = dy.value + dy.stp * diff / 1000;
        if (v < dy.min) {
            var overshoot = dy.min - v;
            if (dy.wr) {// wrap
                v = dy.max - overshoot;
            } else {// bounce
                v = dy.min + overshoot;
                dy.stp = -dy.stp;
            }
        } else if (v > dy.max) {
            var overshoot = v - dy.max;
            if (dy.wr) {// wrap
                v = dy.min + overshoot;
            } else {// bounce
                v = dy.max - overshoot;
                dy.stp = -dy.stp;
            }
        }
        dy.value = v;
    },
    lasttimeinmsec : 0,
    update : function(dt) {
        if (!dt)
            dt = new Date();
        var timeinmsec = dt.getTime();
        var diff = 1000;
        if (this.lasttimeinmsec != 0) {
            diff = timeinmsec - this.lasttimeinmsec;
        }
        this.lasttimeinmsec = timeinmsec;
        timeinmsec -= this.epoch;
        if (this.testfast != undefined) {
            if (this.testfast == 0)
                this.testfast = timeinmsec;
            else
                this.testfast += (((33 * 24 + 1) * 60 + 4) * 60 + 7) * 1000;
            // ms in 33 days, 1hr, 4min, 7sec
            dt = new Date(this.testfast);
            timeinmsec = dt.getTime();
        }
        for (var nm in this.dyna) {//update dynamic variables
            var dynaval = this.dyna[nm];
            if (nm.substr(0, 3) == 'pct') {// pctX or colorX
                this.updateDyna(dynaval, diff)
            } else {
                this.updateDyna(dynaval['dhue'], diff);
                this.updateDyna(dynaval['dsat'], diff);
                this.updateDyna(dynaval['dbrt'], diff);
                dynaval.val = JEB.hslToWeb(dynaval['dhue'].val / 100, dynaval['dsat'].val / 100, dynaval['dbrt'].val / 100);
            }
        }
        if (this.units['year'])
            this.units['year'].value = dt.getFullYear() - this.units['year'].min;
        if (this.units['month'])
            this.units['month'].value = dt.getMonth();
        if (this.units['day']) {
            this.units['day'].value = dt.getDate();
            this.units['month'].max = 32 - new Date(dt.getFullYear(), dt.getMonth(), 32).getDate();
        }
        if (this.units['weekday'])
            this.units['weekday'].value = dt.getDay();
        if (this.units['hour24'])
            this.units['hour24'].value = dt.getHours();
        if (this.units['hour'])
            this.units['hour'].value = this.units['hour24'].value % 12;
        if (this.units['minute'])
            this.units['minute'].value = dt.getMinutes();
        if (this.units['second'])
            this.units['second'].value = dt.getSeconds();
        for (var i in this.units) {
            if (this.units[i].msec && this.units[i].msec != 0) {
                var unit = this.units[i];
                var rng = (unit.max - unit.min + 1);
                unit.value = Math.floor((timeinmsec / unit.msec) % rng);
            }
            // this.fractions[i] = v * 100 / rng;
            // var intV = v + unit.min;
            // this.values[i] = intV;
            // this.strings[i] = unit.strings ? unit.strings[intV - unit.min] : '0' + intV.toString();
        }
        //this.strings['currTime'] = this.toString();
    },
    asVal : function(format) {
        if (format == undefined)
            return undefined;
        if (this.units[format])
            return this.units[format].value;
        // unit reference, eg: {second}
        if (this.dyna[format])
            return this.dyna[format].value;
        // dynamic ref: {pct1} {color3}
        return undefined;
    },
    toString : function(format) {
        if (this.formats[format])//named format, eg: {USnumeric}
            return JEB.substVars(this.formats[format], this);
        var val = this.asVal(format);
        if (this.units[format] && this.units[format].strings)// unit ref: eg. {month:s}
            val = this.units[format].strings[val];
        //field or dyna, eg: {second} or {pct1}
        if (val != undefined)
            return val.toString();
        var s = "";
        for (var i in this.units)
        s += i + ' ' + this.units[i].value + ', ';
        return s;
    },
    toValue : function(format) {
        return this.asVal(format);
    },
    toFraction : function(format) {
        if (this.units[format]) {//scale value to 0..100
            var unit = this.units[format];
            return (unit.value - unit.min) * 100 / (unit.max - unit.min + 1);
        } else//{pct1}
            return this.asVal(format);
    },
}

JEB.genePool = function(opts) {
    return this.init(opts);
}
JEB.genePool.cnt = 0;
JEB.genePool.trans = {
    // @formatter:off
    codonDefs: {
        pctCodons1: "alpha^AA;lx^AB;ty^AC;wide^AD;high^AE;cx^AF;cy^AG;rotate^AH;linewidth^AI;ticstart^AJ;ticlen^AK;value^AL;",
        pctCodons2: "mintic^AM;maxtic^AN;ticstep^AO;ticwidth^AP;top^AR;radius^AS;irad^AT;orad^AU;step^AV;angleoffset^AW",
        pctCodons3: "testfast^AX;xtestfast^AY;refreshtics^AZ;",
        boolCodons: "hidden^BA;fill^BB;draw^BC;drawbase^BD",
        colorCodons: "bgcolor^CA;fillcolor^CB;linecolor^CC;",
        stringCodons: "style^DA;ticalign^DB;text^DC;units^DD",
        numCodons: "minvalue^EA;maxvalue^EB;npts^EC;epoch^ED",
        objCodons: "!timebase^FA;!rect^FB;!marker^FC;!scale^FD;!fraction^FE;",
        dynaCodons1: "dcolor1^GA;dhuestp1^GB;dhuemin1^GC;dhuemax1^GD;dhuewr1^GE;dsatstp1^GF;dsatmin1^GG;dsatmax1^GH;dsatwr1^GI;dbrtstp1^GJ;dbrtmin1^GK;dbrtmax1^GL;dbrtwr1^GM; dpct1^GN; dstp1^GO;dmin1^GP;dmax1^GQ;dwr1^GR;",
        dynaCodons2: "dcolor2^HA;dhuestp2^HB;dhuemin2^HC;dhuemax2^HD;dhuewr2^HE;dsatstp2^HF;dsatmin2^HG;dsatmax2^HH;dsatwr2^HI;dbrtstp2^HJ;dbrtmin2^HK;dbrtmax2^HL;dbrtwr2^HM; dpct2^HN; dstp2^HO;dmin2^HP;dmax2^HQ;dwr2^HR;",
        dynaCodons3: "dcolor3^IA;dhuestp3^IB;dhuemin3^IC;dhuemax3^ID;dhuewr3^IE;dsatstp3^IF;dsatmin3^IG;dsatmax3^IH;dsatwr3^II;dbrtstp3^IJ;dbrtmin3^IK;dbrtmax3^IL;dbrtwr3^IM; dpct3^IN; dstp3^IO;dmin3^IP;dmax3^IQ;dwr3^IR;",
        dynaCodons4: "dcolor4^JA;dhuestp4^JB;dhuemin4^JC;dhuemax4^JD;dhuewr4^JE;dsatstp4^JF;dsatmin4^JG;dsatmax4^JH;dsatwr4^JI;dbrtstp4^JJ;dbrtmin4^JK;dbrtmax4^JL;dbrtwr4^JM; dpct4^JN; dstp4^JO;dmin4^JP;dmax4^JQ;dwr4^JR;",
        dynaCodons5: "dcolor5^KA;dhuestp5^KB;dhuemin5^KC;dhuemax5^KD;dhuewr5^KE;dsatstp5^KF;dsatmin5^KG;dsatmax5^KH;dsatwr5^KI;dbrtstp5^KJ;dbrtmin5^KK;dbrtmax5^KL;dbrtwr5^KM; dpct5^KN; dstp5^KO;dmin5^KP;dmax5^KQ;dwr5^KR;",
        dynaCodons6: "dcolor6^LA;dhuestp6^LB;dhuemin6^LC;dhuemax6^LD;dhuewr6^LE;dsatstp6^LF;dsatmin6^LG;dsatmax6^LH;dsatwr6^LI;dbrtstp6^LJ;dbrtmin6^LK;dbrtmax6^LL;dbrtwr6^LM; dpct6^LN; dstp6^LO;dmin6^LP;dmax6^LQ;dwr6^LR;",
        dynaCodons7: "dcolor7^MA;dhuestp7^MB;dhuemin7^MC;dhuemax7^MD;dhuewr7^ME;dsatstp7^MF;dsatmin7^MG;dsatmax7^MH;dsatwr7^MI;dbrtstp7^MJ;dbrtmin7^MK;dbrtmax7^ML;dbrtwr7^MM; dpct7^MN; dstp7^MO;dmin7^MP;dmax7^MQ;dwr7^MR;",
        dynaCodons8: "dcolor8^NA;dhuestp8^NB;dhuemin8^NC;dhuemax8^ND;dhuewr8^NE;dsatstp8^NF;dsatmin8^NG;dsatmax8^NH;dsatwr8^NI;dbrtstp8^NJ;dbrtmin8^NK;dbrtmax8^NL;dbrtwr8^NM; dpct8^NN; dstp8^NO;dmin8^NP;dmax8^NQ;dwr8^NR;",
        dynaCodons9: "dcolor9^OA;dhuestp9^OB;dhuemin9^OC;dhuemax9^OD;dhuewr9^OE;dsatstp9^OF;dsatmin9^OG;dsatmax9^OH;dsatwr9^OI;dbrtstp9^OJ;dbrtmin9^OK;dbrtmax9^OL;dbrtwr9^OM; dpct9^ON; dstp9^OO;dmin9^OP;dmax9^OQ;dwr9^OR;",
        dynaCodons10: "dcolor10^PA;dhuestp10^PB;dhuemin10^PC;dhuemax10^PD;dhuewr10^PE;dsatstp10^PF;dsatmin10^PG;dsatmax10^PH;dsatwr10^PI;dbrtstp10^PJ;dbrtmin10^PK;dbrtmax10^PL;dbrtwr10^PM; dpct10^PN; dstp10^PO;dmin10^PP;dmax10^PQ;dwr10^PR;",
    },
    valueDefs: {
        pctVals1: "0^aa;1^ab;2^ac;3^ad;4^ae;5^af;6^ag;7^ah;8^ai;9^aj;10^ak;11^al;12^am;13^an;14^ao;15^ap;16^aq;17^ar;18^as;19^at;",
        pctVals2: "20^ba;21^bb;22^bc;23^bd;24^be;25^bf;26^bg;27^bh;28^bi;29^bj;30^bk;31^bl;32^bm;33^bn;34^bo;35^bp;36^bq;37^br;38^bs;39^bt;",
        pctVals3: "40^ca;41^cb;42^cc;43^cd;44^ce;45^cf;46^cg;47^ch;48^ci;49^cj;50^ck;51^cl;52^cm;53^cn;54^co;55^cp;56^cq;57^cr;58^cs;59^ct;",
        pctVals4: "60^da;61^db;62^dc;63^dd;64^de;65^df;66^dg;67^dh;68^di;69^dj;70^dk;71^dl;72^dm;73^dn;74^do;75^dp;76^dq;77^dr;78^ds;79^dt;",
        pctVals5: "80^ea;81^eb;82^ec;83^ed;84^ee;85^ef;86^eg;87^eh;88^ei;89^ej;90^ek;91^el;92^em;93^en;94^eo;95^ep;96^eq;97^er;98^es;99^et;100^eu",
        numVals1: "105^pa;110^pb;115^pc;120^pd;125^pe;130^pf;135^pg;140^ph;155^pi;160^pj;165^pk;170^pl;180^pm;190^pn;195^po;200^pp;205^pq;210^pr;215^ps;220^pt;225^pu;230^pv;235^pw;240^px;245^py;250^pz;",
        negVals1: "-1^na;-2^nb;-3^nc;-4^nd;-5^ne;-6^nf;-7^ng;-8^nh;-9^ni;-10^nj;-11^nk;-12^nl;-13^nm;-14^nn;-15^no;-16^np;-17^nq;-18^nr;-19^ns;-10^nt;",
        negVals2: "-25^oa;-30^ob;-35^oc;-40^od;-45^oe;-50^of;-55^og;-60^oh;-65^oi;-70^oj;-75^ok;-80^ol;-85^om;-90^on;-95^oo;-100^op;-110^oq;-120^or;-125^os;-130^ot;-135^ou;-140^ov;-145^ow;-150^ox;-160^oy;-170^oz;",
        boolnumVals: "true^fa;false^fb;0.5^fc;0.25^fd;0.75^fe;0.2^ff;0.3^fg;0.4^fh;0.6^fi;0.7^fj;0.8^fk;0.9^fl;1.5^fm;2.5^fn;3.5^fo;4.5^fp;5.5^fq;6.5^fr;7.5^fs;8.5^ft;9.5^fu;0.1^fv;",
        colorVals: "#000000^ga;#ffffff^gb;#ff0000^gc;#00ff00^gd;#0000ff^ge;#ffff00^gf;#ff00ff^gg;#00ffff^gh;#808080^gi;#800000^gj;#008000^gk;#000080^gl;#808000^gm;#800080^gn;#008080^go;#0080ff^gp;#00ff80^gq;#8000ff^gr;#ff0080^gs;#80ff00^gt;#ff8000^gu;#ffff80^gv;#ff80ff^gw;#80ffff^gx;#ff8080^gy;#80ff80^gz;#8080ff^g0;",
        stringVals1: "horizontal^ia;vertical^ib;clockwise^ic;counter-cw^id;start^ie;center^if;end^ig;standard^ih;grallt^ii;swatch^ij;style^ik;new^il",
        dynaVals1: "{day|v}^ja;{day|v2}^jb;{day|f}^jc;{day|s}^jd;{weekday|v}^je;{weekday|f}^jf;{weekday|s}^jg;{weekday|s2}^jh;{weekday|s3}^ji;{month|v}^jj;{month|f}^jk;{month|s}^jl;{month|s3}^jm;{year|v}^jn;{year|f}^jo;{hour|v}^jp;{hour|v2}^jq;{hour|f}^jr;{hour|s}^js;{minute|v}^jt;{minute|v2}^ju;{minute|f}^jv;{minute|s}^jw;{second|v}^jx;{second|v2}^jy;{second|f}^jz;{second|v2}^j0;{year|s}^j1;{minute|c2}^j2;{second|c2}^j3;",
        dynaVals2: "{pct1}^ka;{pct2}^kb;{pct3}^kc;{pct4}^kd;{pct5}^ke;{pct6}^kf;{pct7}^kg;{pct8}^kh;{pct9}^ki;{pct10}^kj;",
        dynaVals3: "{color1}^la;{color2}^lb;{color3}^lc;{color4}^ld;{color5}^le;{color6}^lf;{color7}^lg;{color8}^lh;{color9}^li;{color10}^lj;",
        dynaVals4: "{standard|s}^ma;{USnumeric|s}^mb;{USalpha|s}^mc;{USshort|s}^md;{USshortday|s}^me;{USlong|s}^mf;{numeric|s}^mg;",
    },
    AAtoCodon: {},
    codonToAA: {},
    aaToVal: {},
    valToaa: {},
    missingCodons: "",
    missingValues: "",
    geneLen: 2,
    geneChars: 'abcdefghijklmnopqrstuvwxyz',
    nxtCodon: 0,
    nxtValue: 0,
    // @formatter:on
};
JEB.genePool.genes = [];
JEB.genePool.geneIndex = {};
//TODO: analog rings
//TODO: floating digits
JEB.genePool.predef = {
    Vsc : "alpha:100;fillcolor:#ffff80; !scale:new;style:vertical;mintic:10;maxtic:90;minvalue:0;maxvalue:100;ticstep:10;ticstart:4;ticlen:3;ticwidth:0.5; drawbase:true;fillcolor:#00ff00;fill:true;linecolor:#000000;draw:true;  !fraction:new;ticstart:10;ticlen:10;value:50;fillcolor:#0000ff;fill:true;  !timebase:new;xtestFast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:10;wide:90;high:8;fillColor:#000000;fill:false;text:{standard|s};",
    Hsc : "alpha:100;fillcolor:#000000; !scale:new;style:horizontal;mintic:30;maxtic:80;minvalue:0;maxvalue:100;ticstep:25;ticstart:40;ticlen:3;ticwidth:0.5; drawbase:true;fillcolor:#00ff00;fill:true;linecolor:#000000;draw:true;  !fraction:new;ticstart:50;ticlen:10;value:50;fillcolor:#0000ff;fill:true;  !timebase:new;xtestFast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:10;wide:90;high:8;fillColor:#000000;fill:false;text:{standard|s};",
    CWsc : "alpha:100;fillcolor:#ff80ff; !scale:new;style:clockwise;radius:50;mintic:25;maxtic:75;minvalue:0;maxvalue:100;ticstep:8.5;ticstart:0;ticlen:3;ticwidth:0.5;drawbase:true;fillcolor:#ffff00;fill:true;linecolor:#000000;draw:true;  !fraction:new;ticstart:5;ticlen:5;value:25;fillcolor:#8080ff;fill:true; !fraction:new;ticstart:12;ticlen:5;value:75;fillcolor:#00ffff;fill:true;  !timebase:new;xtestFast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:10;wide:90;high:8;fillColor:#000000;fill:false;text:{standard|s};",
    CCWsc : "alpha:100;fillcolor:#80ffff; !scale:new;style:counter-cw;radius:70;mintic:0;maxtic:100;minvalue:0;maxvalue:100;ticstep:8.5;ticstart:-3;ticlen:3;ticwidth:1;drawbase:true;fillcolor:#ff80ff;fill:true;linecolor:#000000;draw:true;  !fraction:new;ticstart:5;ticlen:5;value:50;ticwidth:5;fillcolor:#ff00ff;fill:true; !fraction:new;ticstart:12;ticlen:10;value:75;fillcolor:#00ffff;fill:true; !timebase:new;xtestFast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:10;wide:90;high:8;fillColor:#000000;fill:false;text:{standard|s};",
    Analog : "alpha:100;fillcolor:#ff80ff; !scale:new;alpha:1;style:clockwise;radius:97;mintic:0;maxtic:98;minvalue:0;maxvalue:100;ticstep:8.5;ticstart:0;ticlen:5;ticwidth:0.5;drawbase:false;fillcolor:#000000;fill:true;linecolor:#000000;draw:true;  !fraction:new;alpha:1;ticstart:15;ticlen:85;ticwidth:0.3;value:{second|f};fillcolor:#0000ff;fill:true; !fraction:new;alpha:1;ticstart:25;ticlen:75;ticwidth:0.7;value:{minute|f};fillcolor:#00ff00;fill:true; !fraction:new;alpha:1;ticstart:65;ticlen:35;ticwidth:1.5;value:{hour|f};fillcolor:#ff0000;fill:true; !timebase:new;xtestFast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:10;wide:90;high:8;fillColor:#000000;fill:false;text:{standard|s};",
    Alg2 : "alpha:100;fillcolor:#80ffff; !scale:new;alpha:1;style:clockwise;radius:97;mintic:0;maxtic:98;minvalue:0;maxvalue:100;ticstep:8.5;ticstart:0;ticlen:5;ticwidth:0.5;drawbase:false;fillcolor:#000000;fill:true;linecolor:#000000;draw:true;  !fraction:new;alpha:1;ticstart:15;ticlen:85;ticwidth:0.3;value:{second|f};fillcolor:#0000ff;fill:true; !fraction:new;alpha:1;ticstart:25;ticlen:75;ticwidth:0.7;value:{minute|f};fillcolor:#00ff00;fill:true; !fraction:new;alpha:1;ticstart:65;ticlen:35;ticwidth:1.5;value:{hour|f};fillcolor:#ff0000;fill:true; !timebase:new;xtestFast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:10;wide:90;high:8;fillColor:#000000;fill:false;text:{standard|s};",
    Alg3 : "alpha:100;fillcolor:#ffff80; !scale:new;alpha:1;style:clockwise;radius:97;mintic:0;maxtic:98;minvalue:0;maxvalue:100;ticstep:8.5;ticstart:0;ticlen:5;ticwidth:0.5;drawbase:false;fillcolor:#000000;fill:true;linecolor:#000000;draw:true;  !fraction:new;alpha:1;ticstart:15;ticlen:85;ticwidth:0.3;value:{second|f};fillcolor:#0000ff;fill:true; !fraction:new;alpha:1;ticstart:25;ticlen:75;ticwidth:0.7;value:{minute|f};fillcolor:#00ff00;fill:true; !fraction:new;alpha:1;ticstart:65;ticlen:35;ticwidth:1.5;value:{hour|f};fillcolor:#ff0000;fill:true; !timebase:new;xtestFast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:10;wide:90;high:8;fillColor:#000000;fill:false;text:{standard|s};",
    Alg4 : "alpha:100;fillcolor:#ff80ff; !scale:new;alpha:1;style:clockwise;radius:97;mintic:0;maxtic:98;minvalue:0;maxvalue:100;ticstep:8.5;ticstart:0;ticlen:5;ticwidth:0.5;drawbase:false;fillcolor:#000000;fill:true;linecolor:#000000;draw:true;  !fraction:new;alpha:1;ticstart:15;ticlen:85;ticwidth:0.3;value:{second|f};fillcolor:#0000ff;fill:true; !fraction:new;alpha:1;ticstart:25;ticlen:75;ticwidth:0.7;value:{minute|f};fillcolor:#00ff00;fill:true; !fraction:new;alpha:1;ticstart:65;ticlen:35;ticwidth:1.5;value:{hour|f};fillcolor:#ff0000;fill:true; !timebase:new;xtestFast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:10;wide:90;high:8;fillColor:#000000;fill:false;text:{standard|s};",
    Digital : "alpha:0; !timebase:new;xtestfast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:50;wide:15;high:8;fillColor:#000000;fill:true;linecolor:#80ffff;text:{weekday|s3}; !rect:new;angleoffset:0;lx:26;ty:50;wide:9;high:8;fillColor:#000000;fill:true;linecolor:#80ffff;text:{day|v}; !rect:new;angleoffset:0;lx:36;ty:50;wide:13;high:8;fillColor:#000000;fill:true;linecolor:#80ffff;text:{month|s3}; !rect:new;angleoffset:0;lx:50;ty:50;wide:16;high:8;fillColor:#000000;fill:true;linecolor:#80ffff;text:{year|s}; !rect:new;angleoffset:0;lx:10;ty:40;wide:25;high:8;fillColor:#000000;fill:true;linecolor:#80ffff;text:{hour|s}; !rect:new;angleoffset:0;lx:36;ty:40;wide:10;high:8;fillColor:#000000;fill:true;linecolor:#80ffff;text:{minute|c2}; !rect:new;angleoffset:0;lx:47;ty:40;wide:10;high:8;fillColor:#000000;fill:true;linecolor:#80ffff;text:{second|c2};",
    Dig2 : "alpha:0; !timebase:new;xtestfast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:50;wide:15;high:8;fillColor:#000000;fill:true;linecolor:#ff80ff;text:{weekday|s3}; !rect:new;angleoffset:0;lx:26;ty:50;wide:9;high:8;fillColor:#000000;fill:true;linecolor:#ff80ff;text:{day|v}; !rect:new;angleoffset:0;lx:36;ty:50;wide:13;high:8;fillColor:#000000;fill:true;linecolor:#ff80ff;text:{month|s3}; !rect:new;angleoffset:0;lx:50;ty:50;wide:16;high:8;fillColor:#000000;fill:true;linecolor:#ff80ff;text:{year|s}; !rect:new;angleoffset:0;lx:10;ty:40;wide:25;high:8;fillColor:#000000;fill:true;linecolor:#ff80ff;text:{hour|s}; !rect:new;angleoffset:0;lx:36;ty:40;wide:10;high:8;fillColor:#000000;fill:true;linecolor:#ff80ff;text:{minute|c2}; !rect:new;angleoffset:0;lx:47;ty:40;wide:10;high:8;fillColor:#000000;fill:true;linecolor:#ff80ff;text:{second|c2};",
    Dig3 : "alpha:0; !timebase:new;xtestfast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:50;wide:15;high:8;fillColor:#000000;fill:true;linecolor:#ffff80;text:{weekday|s3}; !rect:new;angleoffset:0;lx:26;ty:50;wide:9;high:8;fillColor:#000000;fill:true;linecolor:#ffff80;text:{day|v}; !rect:new;angleoffset:0;lx:36;ty:50;wide:13;high:8;fillColor:#000000;fill:true;linecolor:#ffff80;text:{month|s3}; !rect:new;angleoffset:0;lx:50;ty:50;wide:16;high:8;fillColor:#000000;fill:true;linecolor:#ffff80;text:{year|s}; !rect:new;angleoffset:0;lx:10;ty:40;wide:25;high:8;fillColor:#000000;fill:true;linecolor:#ffff80;text:{hour|s}; !rect:new;angleoffset:0;lx:36;ty:40;wide:10;high:8;fillColor:#000000;fill:true;linecolor:#ffff80;text:{minute|c2}; !rect:new;angleoffset:0;lx:47;ty:40;wide:10;high:8;fillColor:#000000;fill:true;linecolor:#ffff80;text:{second|c2};",
    Dig4 : "alpha:0; !timebase:new;xtestfast:0;units:standard; !rect:new;angleoffset:0;lx:10;ty:50;wide:15;high:8;fillColor:#000000;fill:true;linecolor:#ffffff;text:{weekday|s3}; !rect:new;angleoffset:0;lx:26;ty:50;wide:9;high:8;fillColor:#000000;fill:true;linecolor:#ffffff;text:{day|v}; !rect:new;angleoffset:0;lx:36;ty:50;wide:13;high:8;fillColor:#000000;fill:true;linecolor:#ffffff;text:{month|s3}; !rect:new;angleoffset:0;lx:50;ty:50;wide:16;high:8;fillColor:#000000;fill:true;linecolor:#ffffff;text:{year|s}; !rect:new;angleoffset:0;lx:10;ty:40;wide:25;high:8;fillColor:#000000;fill:true;linecolor:#ffffff;text:{hour|s}; !rect:new;angleoffset:0;lx:36;ty:40;wide:10;high:8;fillColor:#000000;fill:true;linecolor:#ffffff;text:{minute|c2}; !rect:new;angleoffset:0;lx:47;ty:40;wide:10;high:8;fillColor:#000000;fill:true;linecolor:#ffffff;text:{second|c2};",
    Mover : "alpha:0; !timebase:new;xtestfast:0;units:standard; !rect:new;lx:50;ty:51;cx:50;cy:50;rotate:{second|f};wide:40;high:2;fillColor:#ffffff;fill:true;draw:false; !rect:new;lx:10;ty:10;wide:90;high:8;fillColor:#000000;draw:false;fill:false;text:{standard|s};",
    Rect1 : "alpha:0; !timebase:new;units:standard;dcolor1:#ff00ff;dsatmin1:50;dbrtmin1:30;dhuestp1:17;dcolor2:#00ffff;dsatmin2:50;dbrtmax2:70;dpct1:37;dstp1:-3;dpct2:14;dstp2:13;dpct3:28;dstp3:7;dmin3:5;dmax3:40;dpct4:8;dstp4:-2;dmin4:5;dmax4:40; !rect:new;lx:{pct1};ty:{pct2};wide:{pct3};high:{pct4};fillColor:{color1};fill:true;lineColor:{color2};draw:true;",
    Rect2 : "alpha:0;refreshtics:100; !timebase:new;units:standard;dpct1:37;dpct2:70; !rect:new;rotate:0;angleoffset:0;lx:{pct1};ty:100;wide:0.1;high:100;lineColor:0;draw:true;!rect:new;rotate:0;angleoffset:0;lx:0;ty:{pct2};wide:100;high:0.1;lineColor:0;draw:true;",
};
JEB.genePool.prototype = {
    init : function() {
        this.name = 'genePool';
        this.cnt = 0;
        this.initTranslator();
    },
    initTranslator : function() {
        if (JEB.genePool.trans.AAtoCodon['AA'])
            return;
        for (var i in JEB.genePool.trans.codonDefs) {
            this.defCodons(JEB.genePool.trans.codonDefs[i])
        }
        for (var i in JEB.genePool.trans.valueDefs) {
            this.defValues(JEB.genePool.trans.valueDefs[i])
        }
    },
    addGene : function(gene, name) {
        if (!name)
            name = 'G' + JEB.genePool.cnt;
        this.cnt++;
        JEB.genePool.genes.push({
            name : name,
            gene : gene,
            drawing : null,
        });
        JEB.genePool.geneIndex[name] = JEB.genePool.genes.length;
        gene = gene.trim();
        var trans = this.translateFromGene(gene);
        var calcgene = this.translateToGene(trans);
        if (calcgene != gene)
            JEB.dbg('alert', "gene '{0}' back translation differs: '{1}'".format(name, calcgene));
    },
    byName : function(name) {
        return JEB.genePool.genes[JEB.genePool.geneIndex[name]];
    },
    asCodon : function(idx) {
        return this.asValue(idx).toUpperCase();
    },
    asValue : function(idx) {
        var rdx = JEB.genePool.trans.geneChars.length;
        var g = '';
        while (idx >= rdx) {
            g = g + JEB.genePool.trans.geneChars.charAt(Math.floor(idx / rdx));
            idx = Math.floor(idx % rdx);
        }
        g = g + JEB.genePool.trans.geneChars.charAt(idx);
        while (g.length < JEB.genePool.trans.geneLen) {
            g = JEB.genePool.trans.geneChars.charAt(0) + g;
        }
        return g;
    },
    nextChapter : function(idx) {
        var rdx = JEB.genePool.trans.geneChars.length;
        if (idx % rdx == 0)
            return idx;
        return (Math.floor(idx / rdx) + 1) * rdx;
    },
    showGenes : function(idx, sz, gap) {
        var tp = $('#jeb-header').position().top + $('#jeb-header').outerHeight();
        gap = gap || 2;
        var lft = gap;
        for (var i = idx; i < this.cnt; i++) {
            var g = JEB.genePool.genes[i];
            this.showGeneInPanel(g.name, tp, lft, sz);
            lft = lft + sz + gap;
            if (lft + sz > $('body').width()) {
                lft = gap;
                var ht = $('#' + g.name + '-panel').outerHeight() + gap;
                tp = tp + ht;
                if (tp + ht > $('body').height())
                    return i;
            }
        }
        return this.cnt;
    },
    showGeneInPanel : function(name, tp, lft, sz) {
        JEB.addPanel(name, JEB.clockHtml(name));
        JEB.placeClock(name, tp, lft, sz);
        this.showGene(name + '-canvas', name);
        JEB.panelOpen(name);
    },
    showGene : function(canvasID, name) {
        var canvas = document.getElementById(canvasID);
        if (!canvas)
            return;
        var g = this.byName(name);
        if (!g)
            return;

        var opts = this.translateFromGene(g.gene).split('!');
        g.drawing = new JEB.drawing(canvas, opts[0]);
        for (var i = 1; i < opts.length; i++)
            if (opts[i].length > 0) {
                var el = opts[i];
                el += ';';
                var iSemi = el.indexOf(';');
                var nmval = el.substring(0, iSemi).split(':');
                var elType = nmval[0];
                var options = el.substr(iSemi + 1);
                this.newEl(g, elType, options);
            }
        g.drawing.refresh(g.drawing);
        return g;
    },
    defCodons : function(def) {
        var c = def.split(';');
        JEB.genePool.trans.nxtCodon = this.nextChapter(JEB.genePool.trans.nxtCodon);
        for (var i in c) {
            var cdAA = c[i].split('^');
            //TEMP
            //if (cdAA.length > 1) {
            var cd = cdAA[0].trim().toLowerCase();
            if (cd != '') {
                var AA = this.asCodon(JEB.genePool.trans.nxtCodon++);
                //cdAA[1].trim();
                JEB.genePool.trans.AAtoCodon[AA] = cd;
                JEB.genePool.trans.codonToAA[cd] = AA;
            }
            //}
        }
    },
    defValues : function(def) {
        var v = def.split(';');
        JEB.genePool.trans.nxtValue = this.nextChapter(JEB.genePool.trans.nxtValue);
        for (var i in v) {
            var vlaa = v[i].split('^');
            //if (vlaa.length > 1) {
            var vl = vlaa[0].trim().toLowerCase();
            if (vl != '') {
                var aa = this.asValue(JEB.genePool.trans.nxtValue++);
                //vlaa[1].trim();
                JEB.genePool.trans.aaToVal[aa] = vl;
                JEB.genePool.trans.valToaa[vl] = aa;
            }
            //}
        }
    },
    translateToGene : function(options) {
        var els = options.split(";");
        for (var i in els) {
            if (els[i].trim().length > 0) {
                var nmval = els[i].split(":");
                var nm = nmval[0].trim().toLowerCase();
                var val = nmval.length > 1 ? nmval[1].trim().toLowerCase() : 'style';
                this.registerGene(nm, val);
                nm = JEB.genePool.trans.codonToAA[nm];
                val = JEB.genePool.trans.valToaa[val];
                els[i] = nm + val;
            }
        }
        return els.join('');
    },
    translateFromGene : function(gene) {
        var opts = "";
        var i = 0;
        var gl = JEB.genePool.trans.geneLen;
        while (i < gene.length) {
            var cd = gene.substr(i, gl);
            var vl = gene.substr(i + gl, gl);
            if (cd.toUpperCase() == cd && vl.toLowerCase() == vl) {//valid codon/value
                var codon = JEB.genePool.trans.AAtoCodon[cd];
                var value = JEB.genePool.trans.aaToVal[vl];
                if (codon != undefined && value != undefined)
                    opts += codon + ':' + value + ';';
                i += 4;
            } else {
                i += 1;
            }
        }
        return opts;
    },
    registerGene : function(nm, val) {
        nm = nm.trim().toLowerCase();
        if (!JEB.genePool.trans.codonToAA[nm]) {
            JEB.genePool.trans.missingCodons += "'" + nm + "'  ";
            JEB.genePool.trans.codonToAA[nm] = 'ZZ';
            JEB.genePool.trans.AAtoCodon['ZZ'] = 'MISSING';
            JEB.dbg("gene", "missing Codons: " + JEB.genePool.trans.missingCodons);
        }
        val = val.trim().toLowerCase();
        if (!JEB.genePool.trans.valToaa[val]) {
            JEB.genePool.trans.missingValues += val + ";";
            JEB.genePool.trans.valToaa[val] = 'zz';
            JEB.genePool.trans.aaToVal['zz'] = 'missing';
            JEB.dbg("gene", "missing Values: " + JEB.genePool.trans.missingValues);
        }
    },
    close : function(g) {
        g.drawing.refreshtics = -1;
    },
    newEl : function(gene, eltype, opts) {
        switch (eltype) {
            case 'timebase':
                gene.drawing.dataSource = new JEB.timebase(opts);
                break;
            case 'rect':
                gene.currEl = new JEB.rect(gene.drawing, gene.drawing, opts);
                break;
            case 'marker':
                gene.currEl = new JEB.marker(gene.drawing, gene.drawing, opts);
                break;
            case 'scale':
                gene.currScale = new JEB.scale(gene.drawing, gene.drawing, opts);
                gene.currEl = this.currScale;
                break;
            case 'fraction':
                gene.currEl = new JEB.fraction(gene.drawing, gene.drawing, gene.currScale, opts);
                break;
        }

    },
}

//TODO: save/load clock def
$(function() {
    $(document).on("deviceready", function() {
        JEB.onDeviceReady();
    });
    $('body').on("deviceready", function() {
        JEB.onDeviceReady();
    });

    $('.jebPanel').hide();
    JEB.dbg('dbg', ' start PPG');
    JEB.Header = false;

    function showPage(nm) {
        $('.jebPage').hide();
        // hide all
        $('#' + nm + '-page').show();
    }

    showPage('Home');
    JEB.placePanels();

    var gp = new JEB.genePool();
    var firstIndex = 0, lastIndex = 0, panelSize = 200;
    for (var g in JEB.genePool.predef) {
        gp.addGene(gp.translateToGene(JEB.genePool.predef[g]), g);
    }

    function doFootMenu(sel) {
        switch (sel) {
            case ":[":
                break;
            case ":4":
                //$('#footMenu').show('slow');
                JEB.menuOpen('mainMenu', mainMenuClick);
                break;
            case ":]":
                break;
        }
    }

    function clickMeter(targ, event) {
        var off = 5;
        var min = Math.floor($(targ).offset().left + off);
        var wide = Math.floor($(targ).width() - 2 * off);
        if (wide <= 0) {
            JEB.dbg('weight', 'targ: #{0} {1}'.format(targ.id, targ.tag));
        }
        var pct = Math.floor((event.pageX - min) * 100 / wide);
        if (pct < 0)
            pct = 0;
        if (pct > 100)
            pct = 100;
        JEB.dbg('weight', 'min={0},wide={1},pX={3},clicked at {0}%'.format(min, wide, event.pageX, pct));
        $(targ).children('div').css('width', pct + '%');
    }


    JEB.onTouchClick('body', '.jebMeter', clickMeter);
    //TODO: auto after new .meter?

    function footMenuClick(targ, event) {
        doFootMenu(':' + $(targ).text());
    }


    JEB.onTouchClick('.jebFoot', 'a', footMenuClick);
    $('body').on('swipe keypress tap click', function(event) {
        console.log('got ' + event.type);
        return false;
    });

    // @formatter:off
    var mainMenu = 
    [
        'Clocks', // ['Clocks', 2, Object.keys(JEB.genePool.genes)], 
        ['Panels', 1, 'LoadMsg;Debug;Clock;Table;Edit'], 
        ['SetSize', -3, '6;7;8;9;10;11;12;13;14;15;16;17;18;19;20'], 
        'Home', 
        'AboutUs', 
        'Test', 
        'Other'
     ];
     if (JEB.Debug) mainMenu.push( JEB.dbgMenu  );
    // @formatter:on
    function mainMenuClick(menuPath, targ) {
        var cmd = menuPath.split(':');
        switch (cmd[1]) {
            case 'Clocks':
                JEB.dbg('menu', 'Clocks');
                // if (currGene != null)
                // gp.close(currGene);
                // $('#Clock').show('slow');
                //currGene = gp.showGene('jeb-Canvas', cmd[2]);

                lastIndex = gp.showGenes(firstIndex, panelSize);
                JEB.setHeader('Displaying clocks {0}..{1}'.format(firstIndex,lastIndex));
                // JEB.addPanel('test', JEB.clockHtml('test'));
                // JEB.placeClock('test', tp, lft, 200);
                // currGene = gp.showGene('test-canvas', cmd[2]);
                // JEB.panelOpen('test-canvas');
                break;
            case 'Panels':
                JEB.dbg('menu', 'togglePanel({0})'.format(cmd[2]));
                $(targ).toggleClass('jebSel');
                if ($(targ).hasClass('jebSel'))
                    $('#jeb-' + cmd[2]).show('slow');
                else
                    $('#jeb-' + cmd[2]).hide('fast');

                switch (cmd[2]) {
                    case 'Table':
                        JEB.getGeo();
                        JEB.devFiller = new JEB.filler(DeviceDescriptor, 'jeb-Table', function(result) {
                            User.Report = JEB.devFiller.fResult;
                            $('#jeb-Table').html(JEB.genTableHtml(User));
                            User.Time = new Date().toDateString();
                            if (window.XMLHttpRequest) {
                                var aj = new XMLHttpRequest();
                                aj.open("POST", "http://winenotes.biz/log/logger.asp", true);
                                aj.send(JSON.stringify(User));
                            }
                        });
                        break;
                }
                break;
            case 'SetSize':
                JEB.dbg('menu', 'setFontSize({0})'.format(cmd[2]));
                //TODO: deselect current size
                $(targ).toggleClass('jebSel');
                JEB.dbg('setSize', 'font-size: {0}px'.format(cmd[2]));
                var bodycss = {
                    'font-size' : cmd[2] + 'px'
                };
                $('html div').css(bodycss);
                break;
            case 'Home':
            case 'AboutUs':
            case 'Test':
                // history.pushState('jeb-home', $('#jeb-home').attr('title'), 'jeb-home');
                JEB.dbg('menu', 'show page ' + cmd[1]);
                showPage(cmd[1]);
                break;
            case 'Other':
                JEB.dbg('menu', 'Other()');
                window.location.href = "http://villaduvin.com/vdv/PPG/Other/";
                break;
            case 'Debug':
                JEB.dbg('menu', 'Debug:' + cmd[2]);
                JEB.dbgCmd(cmd[2]);
                break;
        }
    };
    JEB.buildMenus('mainMenu', 1, mainMenu);
    $('body').listHandlers('*', console.info);

    $(window).resize(function() {
        JEB.placePanels();
    });
});
